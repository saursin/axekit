include(ExternalProject)

set(ext_INCLUDE ${CMAKE_SOURCE_DIR}/ext/include)
set(ext_LIB ${CMAKE_SOURCE_DIR}/ext/lib)
#-Debug later---set(ext_BOOST ${CMAKE_SOURCE_DIR}/ext/boost)

file(MAKE_DIRECTORY ${ext_INCLUDE})
file(MAKE_DIRECTORY ${ext_LIB})
#-Debug later---file(MAKE_DIRECTORY ${ext_BOOST})


add_custom_target(
  external ALL
  ${CMAKE_COMMAND} -E echo_append ""
  COMMENT "Build external dependencies"
)

#-------------------------------------------------------------------------------
# Check if readline is available.
include(CheckCXXSourceRuns)

set(CMAKE_REQUIRED_LIBRARIES readline)
check_cxx_source_runs("
#include <stdio.h>
#include <readline/readline.h>
void foo() { readline(\"foo\"); }
int main() { return 0; }
" READLINE_WORKS)
unset(CMAKE_REQUIRED_LIBRARIES)

if( READLINE_WORKS )
  set(USE_READLINE 1)
  set(READLINE_LIBRARY readline)
  add_definitions(-DUSE_READLINE=1)
else( )
  set(USE_READLINE 0)
  set(READLINE_LIBRARY)
endif( )
#-------------------------------------------------------------------------------
# Build ABC
# All options relative to "build" directory.

set(abc_SRC ${CMAKE_BINARY_DIR}/ext/abc-prefix/src)
externalProject_add(abc
  DOWNLOAD_DIR .
  URL "http://msoeken.github.io/tools/alanmi-abc-394fe1f315ae.zip"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND make -j${ProcCount} ABC_USE_PIC=1 ABC_USE_NO_READLINE=1 ABC_USE_NAMESPACE=abc OPTFLAGS=-O3 libabc.a
  BUILD_IN_SOURCE 1
  INSTALL_COMMAND cp ${abc_SRC}/abc/libabc.a ${ext_LIB}/libabc.a
  # ABC is not packaged with the libraries as before.
  # Must inclue abc_SRC in the top level program build also.
  # TODO: solve this later.
  #INSTALL_COMMAND rsync --include "*.h" --filter "hide,! */" -avm ${abc_SRC}/abc/src/ ${ext_INCLUDE}/ COMMAND cp ${abc_SRC}/abc/libabc.a ${ext_LIB}/libabc.a
  LOG_DOWNLOAD 1
  LOG_BUILD 1
  LOG_INSTALL 1)
add_dependencies(external abc)



find_library(DL_LIBRARY dl)
set(abc_LIBRARIES
  ${ext_LIB}/libabc.a
  ${DL_LIBRARY}
  pthread
  ${READLINE_LIBRARY}

  PARENT_SCOPE
)

add_dependencies(external abc)
#add_definitions(-DABC_NAMESPACE=abc)

#-------------------------------------------------------------------------------
# Build Yosys
set(patch_YOSYS  ${CMAKE_SOURCE_DIR}/ext/patches/yosys/yosys.patch)
set(yosys_SRC ${CMAKE_BINARY_DIR}/ext/yosys-prefix/src/)
externalProject_add(yosys
  DOWNLOAD_DIR .
  GIT_REPOSITORY "https://github.com/cliffordwolf/yosys.git"
  GIT_TAG "6523023"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND make config-gcc COMMAND make
  INSTALL_COMMAND cp ${yosys_SRC}/yosys/libyosys.so ${ext_LIB}/
  BUILD_IN_SOURCE 1
  LOG_DOWNLOAD 1
  LOG_BUILD 1
  LOG_INSTALL 1
  PATCH_COMMAND patch Makefile ${patch_YOSYS}
  )
set( yosys_LIBRARIES ${ext_LIB}/libyosys.so PARENT_SCOPE)
add_dependencies(external yosys)


#-------------------------------------------------------------------------------
# Build CryptoMinisat
#set(patch_CRYPTO  ${CMAKE_SOURCE_DIR}/ext/patches/cryptominisat/crypto.patch)
set(cryptominisat_SRC ${CMAKE_BINARY_DIR}/ext/cryptominisat-prefix/src)
set(cryp ${cryptominisat_SRC}/cryptominisat)
externalProject_add(cryptominisat
  DOWNLOAD_DIR .
  GIT_REPOSITORY "https://github.com/msoos/cryptominisat.git"
  GIT_TAG "de8c6cd"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND  cmake -DUSE_GAUSS=1 . COMMAND make preinstall COMMAND make scalmc COMMAND make libcryptominisat5
  INSTALL_COMMAND cp ${cryp}/lib/libcryptominisat5.so ${ext_LIB}/  COMMAND cp ${cryp}/lib/libcryptominisat5.so.5.0 ${ext_LIB}/  COMMAND rm -rf ${ext_INCLUDE}/cryptominisat5  COMMAND  cp -rf ${cryp}/include/cryptominisat5 ${ext_INCLUDE}/
  BUILD_IN_SOURCE 1
  LOG_DOWNLOAD 1
  LOG_BUILD 1
  LOG_INSTALL 1
#  PATCH_COMMAND patch Makefile ${patch_CRYPTO}
  )
set( crypto_LIBRARIES ${ext_LIB}/libcryptominisat5.so PARENT_SCOPE)
add_dependencies(external cryptominisat)



#-------------------------------------------------------------------------------
# Build SharpSAT
set(patch_SHARP  ${CMAKE_SOURCE_DIR}/ext/patches/sharpsat/sharpsat.patch)
set(SHARPSAT_SRC ${CMAKE_BINARY_DIR}/ext/sharpSAT-prefix/src)
set(sharp ${SHARPSAT_SRC}/sharpSAT)
externalProject_add(sharpSAT
  DOWNLOAD_DIR .
  GIT_REPOSITORY "https://github.com/marcthurley/sharpSAT.git"
  GIT_TAG "e57881a"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND  cmake -DCMAKE_BUILD_TYPE=Release . COMMAND make 
  INSTALL_COMMAND cp ${sharp}/libsharpsat.12.08.so ${ext_LIB}/ 
  BUILD_IN_SOURCE 1
  LOG_DOWNLOAD 1
  LOG_BUILD 1
  LOG_INSTALL 1
  PATCH_COMMAND patch CMakeLists.txt ${patch_SHARP}
  )
set( sharpSAT_LIBRARIES ${ext_LIB}/libsharpsat.12.08.so PARENT_SCOPE)
add_dependencies(external sharpSAT)



#-------------------------------------------------------------------------------
# Build CUDD
set( cudd_SRC ${CMAKE_BINARY_DIR}/ext/cudd-prefix/src/cudd)
set( cudd_INCLUDE_FILES
  ${cudd_SRC}/config.h
  ${cudd_SRC}/cudd/cudd.h
  ${cudd_SRC}/cudd/cuddInt.h
  ${cudd_SRC}/cplusplus/cuddObj.hh
  ${cudd_SRC}/epd/epd.h
  ${cudd_SRC}/epd/epdInt.h
  ${cudd_SRC}/mtr/mtr.h
  ${cudd_SRC}/mtr/mtrInt.h
  ${cudd_SRC}/st/st.h
  ${cudd_SRC}/util/cstringstream.h
  ${cudd_SRC}/util/util.h
  )

set( cudd_LIBRARY_FILES
  ${cudd_SRC}/cudd/.libs/libcudd.so
  ${cudd_SRC}/cudd/.libs/libcudd-3.0.0.so.0
  ${cudd_SRC}/cudd/.libs/libcudd-3.0.0.so.0.0.0
  )

include(ProcessorCount)
ProcessorCount(ProcCount)
if( ProcCount EQUAL 0 )
  set( ProcCount 1 )
endif()

externalProject_add(cudd
  DOWNLOAD_DIR .
  URL "http://msoeken.github.io/tools/cudd-3.0.0.tar.gz"
  CONFIGURE_COMMAND ./configure --enable-obj --enable-shared --enable-dddmp
  BUILD_COMMAND make -j${ProcCount}
  INSTALL_COMMAND cp ${cudd_INCLUDE_FILES} ${ext_INCLUDE} COMMAND cp -L ${cudd_LIBRARY_FILES} ${ext_LIB}
  BUILD_IN_SOURCE 1
  LOG_DOWNLOAD 1
  LOG_BUILD 1
  LOG_INSTALL 1)
add_dependencies(external cudd)

set(CUDD_LIBRARIES
  ${ext_LIB}/libcudd${CMAKE_SHARED_LIBRARY_SUFFIX}

  PARENT_SCOPE
)

#-------------------------------------------------------------------------------
