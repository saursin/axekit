// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : system_path.hpp
// @brief  : pwd shows the current path, cd changes the directory.
//
//------------------------------------------------------------------------------
#pragma once

#ifndef SYSTEM_PATH_HPP
#define SYSTEM_PATH_HPP

#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

namespace alice {

class pwd_command : public command {
public:
  pwd_command (const environment::ptr &env) : command (env, "Show current directory")
  {
    opts.add_options()
      ;
  }
protected:
  // the main execution part of the command.
  bool execute() {
    curr_path = boost::filesystem::current_path().string();
    std::cout << curr_path << "\n";
    return true;
  }
  // Log for tool execution in JSON format
  log_opt_t log() const {
    return log_opt_t (
      {
	{"pwd", curr_path}
      }
      );
  }

private:
  std::string curr_path;
  
};


//----------------------------------------------------------------------------------------

// rule 1
inline bool _dir_exists_ (const std::string &new_path) {
  boost::filesystem::path p(new_path);
  if ( boost::filesystem::exists (p) ) {
    return true;
  }
  return false;
}

// rule 2
inline bool _is_dir_ (const std::string &new_path) {
  boost::filesystem::path p(new_path);
  return boost::filesystem::is_directory(p);
}

class cd_command : public command {
public:
  cd_command (const environment::ptr &env) : command (env, "Change AxC working directory")
  {
    add_positional_option ("new_path");
    opts.add_options()
      ( "new_path,n", po::value (&new_path),              "new directory" )
      ;
  }
protected:
  rules_t validity_rules() const{
    return {
      { [this]() { return _dir_exists_ (new_path); } , new_path + " does not exist!"  },
      { [this]() { return _is_dir_ (new_path); } , new_path + " is not a directory!"  }
    };
  }

  // the main execution part of the command.
  bool execute() {
    boost::filesystem::path p(new_path);
    old_path = boost::filesystem::current_path().string();
    boost::filesystem::current_path(p);
    return true;
  }
  // Log for tool execution in JSON format
  log_opt_t log() const {
    return log_opt_t (
      {
	{"new_dir", new_path},
	{"old_dir", old_path}
      }
      );
  }

private:
  std::string new_path, old_path;
  
};


}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
