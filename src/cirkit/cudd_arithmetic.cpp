// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cudd_arithmetic.cpp
// @brief  : Arithemtic functions using CUDD BDD (adder, subtractor) etc
//------------------------------------------------------------------------------


#include "cudd_arithmetic.hpp"
#include <cuddInt.h>
#include <cstdio>
#include <gmpxx.h>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>

namespace axekit
{

using namespace cirkit;


std::pair <BDD, BDD> full_adder (const BDD &x, const BDD &y, const BDD&cin) {

  auto sum = x ^ y ^ cin;
  auto cout = (x * y) + (x * cin) + (y * cin);
  return {sum, cout};
}


std::pair < std::vector<BDD>, BDD > full_adder ( const std::vector<BDD> &x, const std::vector<BDD> &y,
						 const std::vector<BDD> &cin ) {

  std::vector<BDD> sum;
  assert (x.size() == y.size());
  assert (cin.size() == 1);

  auto cx = cin[0];
  for ( auto i=0u; i<x.size(); i++ ) {
    sum.emplace_back ( x[i] ^ y[i] ^ cx );
    //cx = (x[i] * y[i]) + (x[i] * cx ) + (y[i] * cx);
    cx = (x[i] * y[i]) + ( cx * (x[i] ^ y[i]) );
  }
  auto cout = cx;
  return {sum, cout};
}

// result is <sum, carryout>
std::pair <bdd_function_t, bdd_function_t>
full_adder (const bdd_function_t &x, const bdd_function_t &y, const bdd_function_t &cin) {
  assert ( x.second.size() == y.second.size() );
  assert ( cin.second.size() == 1 );
  auto xbdd = x.second;
  auto ybdd = y.second;
  auto cinbdd = cin.second;

  auto result = full_adder(xbdd, ybdd, cinbdd);

  bdd_function_t sum = {x.first, result.first};
  bdd_function_t carry = {x.first , {result.second}};
  
  return {sum, carry};
  
}

// result is <difference, borrow>
std::pair <bdd_function_t, bdd_function_t>
subtract_bdd (const bdd_function_t &x, const bdd_function_t &y) {
  assert ( x.second.size() == y.second.size() );
  // 2's compliment
  auto result = full_adder ( x, not_bdd (y), { x.first, {x.first.bddOne()} } );
  return result;
}


// input: first is diff, second is carry.
bdd_function_t abs_bdd ( const std::pair <bdd_function_t, bdd_function_t> &x ) {
  //assert (false && "There is a bug in abs_bdd()"); Come back to this bug later.
  auto sum = x.first.second;
  auto carry = x.second.second;
  assert (sum.size() > 0);
  assert (carry.size() == 1);
  auto mgr = x.first.first;
  auto cy = carry.back();
  auto maskval = !cy;
  
  std::vector<BDD> mask ( sum.size(), maskval );
  std::vector<BDD> xormask ( sum.size() );
  for ( auto i=0u; i<xormask.size(); i++ ) {
    xormask[i] = sum[i] ^ mask[i];
  }
  assert ( xormask.size() ==  mask.size() );

  std::vector<BDD> addval( sum.size(), mgr.bddZero() );
  addval[0] = maskval;
  
  auto result = full_adder ( xormask, addval, {mgr.bddZero()} );
  return  {mgr, result.first};
}


boost::multiprecision::uint256_t get_max_value ( const bdd_function_t &bddf ) {
  auto bdds = bddf.second;
  auto ddMgr = bddf.first.getManager();
  assert ( !bdds.empty() );
  
  boost::dynamic_bitset<> bs ( bdds.size() );
  auto mask = bddf.first.bddOne();

  for ( int k = bdds.size() - 1; k >= 0;  --k ) {
    auto r = mask * bdds[k];
    // if ( 0 == r.NodeReadIndex() ) { // this is wrong.
    if ( r.getNode() != Cudd_Not ( DD_ONE (ddMgr) ) ) {
      bs.set ( k );
      mask = r;
    }
  }
  return to_multiprecision <boost::multiprecision::uint256_t> (bs);
}


boost::multiprecision::uint256_t get_max_value_chi ( const bdd_function_t &bddf ) {
  auto bdds = bddf.second;
  assert ( !bdds.empty() );
  auto bddrev = bdds;
  boost::reverse (bddrev);

  //auto chif = characteristic_function ( {bddf.first, bddrev} ); // this is wrong, use cirkit function
  auto chif = compute_characteristic ( {bddf.first, bddrev}, false );
  assert (chif.second.size() == 1 && "Characteristic function is not a single BDD!");
  assert (!chif.second.empty() && "Characteristic function is empty!");
  auto chiBDD = chif.second[0];

  auto debug_chi = false;
  if (debug_chi) {
    const auto add = chif.second[0].Add();
    FILE *fp = fopen ("chi.dot", "w");
    std::vector<ADD> dot = {add};
    chif.first.DumpDot (dot, 0, 0, fp);
    fclose (fp);
  }

  boost::dynamic_bitset<> bs ( bdds.size() );
  auto chi = chiBDD.getNode();
  auto *chichi = Cudd_Regular( chi ); // donno whts the difference bw regular-node and just node.
  auto nodeZero = chif.first.bddZero().getRegularNode(); // some mnemonic is there for this.

  while ( Cudd_NodeReadIndex(chichi) < bdds.size() ) {
    auto low  = cuddE(chichi);
    auto high = cuddT(chichi);
    if ( high == nodeZero ) {
      chichi = low;
    }
    else {
      bs.set ( bdds.size() -1u - Cudd_NodeReadIndex(chichi) );
      chichi = high;
    }
  }

  return to_multiprecision <boost::multiprecision::uint256_t> (bs);

}

mpz_class pow2(unsigned n)
{
  return mpz_class("1" + std::string(n, '0'), 2);
}


unsigned calculate_required_lines(unsigned n, unsigned m, mpz_class maxmu)
{
  unsigned exp = 0u;
  
  while (pow2(exp) < maxmu) {
    ++exp;
#ifdef DEBUG
    std::cout << "exp: " << exp << std::endl;
#endif
  }
#ifdef DEBUG
  std::cout << "n: " << n << "; m: " << m << std::endl;
#endif
  return n > m + exp ? n : m + exp;
}


void count_output_pattern_recurse( DdManager* mgr, DdNode* node,
				   unsigned num_inputs, unsigned num_outputs,
                                   const std::string& pattern, unsigned depth,
                                   std::vector<mpz_class>& counts, std::vector<std::string>& patterns,
                                   bool verbose )
{
  if ( Cudd_IsConstant( node ) ) { return; }

  if ( depth == num_outputs )
  {
    if ( verbose )
    {
      std::cout << pattern << " has " << Cudd_CountMinterm( mgr, node, num_inputs ) << std::endl;
    }
    patterns += pattern;
    counts += mpz_class( Cudd_CountMinterm( mgr, node, num_inputs ) );
  }
  else
  {
    count_output_pattern_recurse( mgr, Cudd_Regular( cuddT( node ) ), num_inputs, num_outputs, pattern + "1", depth + 1u, counts, patterns, verbose );
    count_output_pattern_recurse( mgr, Cudd_Regular( cuddE( node ) ), num_inputs, num_outputs, pattern + "0", depth + 1u, counts, patterns, verbose );
  }
}


// ToDo: get rid of MPZ class
boost::multiprecision::uint256_t get_weighted_sum ( const bdd_function_t &bddf ) {
  using mpuint = boost::multiprecision::uint256_t;
  auto bdds = bddf.second;
  assert ( !bdds.empty() );
  // if the bdds is all constant, something goofs up while creating the chi.
  // rather than that, take care of it here itself.
  mpuint konstant = 0;
  const mpuint one(1);
  auto konst_count = 0;
  for (auto i=0; i<bdds.size(); i++) {
    auto bdd = bdds[i];
    if ( bdd.IsZero() ) konst_count++;
    if ( (!bdd).IsZero() ) {
      konst_count++;
      konstant = konstant + (one << i);
    }
  }
  if ( konst_count == bdds.size() )  {// every bdd is a constant.
     // every input combination, output is const. (konstant can be 0 also.)
    return konstant * ( one << bddf.first.ReadSize() );
  }

  
  const auto level = bdds.size();
  auto bddrev = bdds;
  boost::reverse (bddrev);
  const auto chif = compute_characteristic ( {bddf.first, bddrev}, false );
  assert (chif.second.size() == 1 && "Characteristic function is not a single BDD!");
  assert (!chif.second.empty() && "Characteristic function is empty!");

  const auto add = chif.second[0].Add();

  auto debug_chi = false;
  if (debug_chi) {
    FILE *fp = fopen ("chi_add_gws.dot", "w");
    std::vector<ADD> dot = {add};
    chif.first.DumpDot (dot, 0, 0, fp);
    fclose (fp);
  }
  
  std::vector<mpz_class> counts;
  std::vector<std::string> patterns;
  const unsigned n = bddf.first.ReadSize();
  const unsigned m = bddf.second.size();
  auto verbose = false;
  count_output_pattern_recurse ( chif.first.getManager(),
				 add.getNode(), n, m, "", 0u, counts, patterns, verbose );

  
  assert (patterns.size() == counts.size());

  mpuint sum(0);
  for (auto i=0u; i<patterns.size(); i++) {
    auto num = to_multiprecision <mpuint> ( boost::dynamic_bitset<> (patterns[i]));
    sum = sum + ( counts[i].get_ui() * num ) ;
  }

// #define DEBUG_GWS
#ifdef DEBUG_GWS
  std::cout << "BDD Patterns : count \n";
  for (auto i=0u; i<patterns.size(); i++) {
    std::cout << patterns[i] << "  :  " << counts[i].get_ui() << "\n";
  }
#endif
  
  return sum;
  //auto result =  calculate_required_lines( n, m, *boost::max_element( counts ) ) - n;
}


// returns a vector {zerobdd_0, zerobdd_1,...zerobdd_num, bdd_1, bdd_0, ...}
inline std::vector<BDD> create_bdd_vector (const bdd_function_t &bddf, unsigned prepend_width) {
  assert ( !bddf.second.empty() && "Empty BDD vector!!!");
  if (0u == prepend_width) return {bddf.second}; // dont have to prepend anything.
  std::vector <BDD> result ( prepend_width + bddf.second.size(), bddf.first.bddZero() );
  for (auto i=0; i<bddf.second.size(); i++) {
    result[i] = bddf.second[i];
  }
  return result;
}


// input = f[n-0]
// output = f[0] + f[1] + f[2] + .. + f[n]
bdd_function_t add_individual_outputs (const bdd_function_t &bddf) {
  auto mgr = bddf.first;
  auto bdds = bddf.second;
  
  std::vector<BDD> bddr = bdds;
  boost::reverse (bddr);

  std::pair < std::vector<BDD>, BDD > result;
  std::vector<BDD> ybdd = {mgr.bddZero()};
  const std::vector<BDD> cin = {mgr.bddZero()};
  
  for (auto i=0u; i<bddr.size(); i++) {
    auto x = create_bdd_vector ( {mgr, {bddr[i]} }, ybdd.size() - 1 ); // prepend so many zerobdd as yb
    result = full_adder (x, ybdd, cin);
    ybdd = result.first;
    ybdd.emplace_back (result.second);
  }
  std::vector<BDD> sumvec = result.first;
  sumvec.emplace_back (result.second);
  bdd_function_t rs = {mgr, sumvec}; // cout as MSB and then sum.
  return rs; 
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
