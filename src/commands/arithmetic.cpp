// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : arithmetic.cpp
// @brief  : Addition, subtraction, absolute value of two functions
//           
//           
//------------------------------------------------------------------------------

#include "arithmetic.hpp"
#include <functions/gia/gia_arithmetic.hpp>
#include <cirkit/cudd_arithmetic.hpp>

namespace alice
{

namespace {
// Private functions.
}

arithmetic_command::rules_t arithmetic_command::validity_rules() const {

  return {
    { [this]() {
	return store == 0u || store == 1u;
      }, "Only Aig/BDD supported currently. Use store = 0/1" },

      
    { [this]() {
	if (1u == store) { // BDD store
	  const auto& bdds = env->store<cirkit::bdd_function_t>();
	  return id1 < bdds.size() && id2 < bdds.size();
	}
	else if (0u == store) { // AIG store
	  auto& gias = env->store<abc::Gia_Man_t*> ();
	  return id1 < gias.size() && id2 < gias.size();	  
	}
	else { // Ntk store
	  return false;
	}
      }, "Store ids are out of range" },

      
    { [this]() {
	return is_set("add") | is_set("sub") | is_set("abs") | is_set("max") ;
      }, "Specify which function to compute." },

      
    { [this]() {
	return  !(
	  ( is_set("add") & is_set("sub") ) |
	  ( is_set("add") & is_set("abs") ) |
	  ( is_set("sub") & is_set("abs") ) )
	  ;
      }, "Only one function can be computed at a time." }
    
  };

}

bool arithmetic_command::execute() {
  if (1u == store) {
    auto& bdds = env->store<cirkit::bdd_function_t> ();
    auto fs = bdds[id1];
    
    if ( is_set ("max") ) {
      std::cout << "[i] max_value            = " << axekit::get_max_value (fs) << "\n";
      return true;
    }
    cirkit::bdd_function_t fshat;
    std::vector<BDD> zerovec = {fs.first.bddZero()};
    cirkit::bdd_function_t  result;
    cirkit::bdd_function_t  zero = {fs.first, zerovec};
    
    if ( is_set ("add") ) {
      fshat = bdds[id2];
      auto sum = axekit::full_adder ( fs, fshat, zero );
      result = sum.first;
      result.second.emplace_back (sum.second.second[0]); // Carry as MSB
    }

    if ( is_set("sub") ) {
      fshat = bdds[id2];
      auto sum = axekit::subtract_bdd ( fs, fshat );
      result = sum.first;
      result.second.emplace_back (sum.second.second[0]); // Carry as MSB
    }

    if ( is_set("abs") ) {
      auto new_bdd = fs;
      cirkit::bdd_function_t cy = { new_bdd.first, {new_bdd.second.back()} };
      new_bdd.second.pop_back();
      cirkit::bdd_function_t diff = new_bdd;
      result = axekit::abs_bdd ({diff, cy});
    }

    if ( is_set("new") || bdds.empty() ) {
      bdds.extend();
    }
    if ( result.second.empty() ) std::cout << "[w] Result is empty BDD vector!\n";
    bdds.current() = result;
    
  }
  
  else if (0u == store) {
    auto& aigs = env->store<abc::Gia_Man_t*> ();
    auto fs = aigs[id1];

    if ( is_set ("max") ) {
      std::cout << "[i] max_value            = " << axekit::get_max_value (fs) << "\n";
      return true;
    }
    
    auto fshat = aigs[id2];
    abc::Gia_Man_t *gia;
    if ( is_set("add") ) {
      std::cout << "[e] Addition in AIG not supported for now!\n";
      return false;
    }
    else if ( is_set("sub") ) {
      std::cout << "[w] Subtraction in AIG always returns abs value\n";
      gia = axekit::subtract_gia ( fs, fshat );
    }
    else if ( is_set ("abs") ) {
      std::cout << "[w] Not supported. --sub in AIG returns abs value if needed\n";
      return false;
    }
    else {
      std::cout << "[e] Invalid operation\n";
      return false;
    }
    
    if ( is_set("new") || aigs.empty() ) {
      aigs.extend();
    } 
    aigs.current() = gia;
  }

  else {
    std::cout << "[e] Invalid store\n";
    return false;
  }
  
  return true;
}


arithmetic_command::log_opt_t arithmetic_command::log() const  {
  return log_opt_t (
    {
      {"function", is_set("add") ? std::string("add") :
	  ( is_set("sub") ? std::string("sub") : std::string("abs") ) },
      {"store_used", (store == 0u) ? std::string("BDD") : std::string("AIG") },
      {"store_id1", id1 },
      {"store_id2", id2 }
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
