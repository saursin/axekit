// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : axekit_stores.hpp
// @brief  : Definition and implementation of all the store-types used in AxeKit
//           Gia, BDD (CUDD) available in the store.
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef AXEKIT_STORES_HPP
#define AXEKIT_STORES_HPP

#define LIN64

#include <base/wlc/wlc.h>
#include <aig/gia/gia.h>
#include <boost/format.hpp>
#include <alice/command.hpp>
#include <alice/alice.hpp>


#include <cstdio>
#include <functional>
#include <iostream>

#include <boost/range/algorithm.hpp>
#include <cirkit/bdd_utils.hpp>
#include <functions/cirkit_routines.hpp>
#include <ext-libs/abc/functions/gia_to_cirkit.hpp>
#include <cirkit/range_utils.hpp>
#include <cirkit/aig_from_bdd.hpp>

namespace alice {
using namespace axekit;
/******************************************************************************
 * store entry abc::Gia_Man_t*                                                *
 ******************************************************************************/

/* register abc::Gia_Man_t* as store element for AIGs */
template<>
struct store_info<abc::Gia_Man_t*> {
  static constexpr const char* key = "aigs"; /* internal key, must be unique for each store */
  static constexpr const char* option = "aig"; /* long flag for general commands, e.g., `store --aig` */
  static constexpr const char* mnemonic = "a"; /* short flag for general commands, e.g., `store -a` */
  static constexpr const char* name   = "AIG";   /* singular name for option descriptions */
  static constexpr const char* name_plural = "AIGs";  /* plural name for option descriptions */
};

/* I/O tag to implement `read_aiger` and `write_aiger` */
//struct io_aiger_tag_t {};

/* return some short text for each AIG in `store -a` */
template<>
inline std::string store_entry_to_string <abc::Gia_Man_t*> ( abc::Gia_Man_t* const& aig ) {
  return boost::str (
    boost::format( "%s   i/o = %d/%d   regs = %d" ) %
    abc::Gia_ManName( aig ) % abc::Gia_ManPiNum( aig ) % abc::Gia_ManPoNum( aig ) % aig->nRegs
    );
}

/* print statistics on `ps -a` */
template<>
inline void print_store_entry_statistics <abc::Gia_Man_t*> ( std::ostream& os,
							     abc::Gia_Man_t* const& aig ) {
  abc::Gps_Par_t Pars;
  memset( &Pars, 0, sizeof(abc::Gps_Par_t) );
  abc::Gia_ManPrintStats( aig, &Pars );
}



/******************************************************************************
 * store entry cirkit::bdd_function_t                                         *
 * CUDD BDD package                                                           *
 ******************************************************************************/

/* register cirkit::bdd_function_t as store element for BDDs */
template<>
struct store_info<cirkit::bdd_function_t> {
  static constexpr const char* key = "bdds"; /* internal key, must be unique for each store */
  static constexpr const char* option = "bdd"; /* long flag for general commands, `store --bdd` */
  static constexpr const char* mnemonic = "b"; /* short flag for general commands,`store -b` */
  static constexpr const char* name   = "BDD";   /* singular name for option descriptions */
  static constexpr const char* name_plural = "BDDs";  /* plural name for option descriptions */
};

//---//----------------------------------------------------------------------------------------
//---/* I/O tag to implement `read_bdd` and `write_bdd` */
//---//struct io_bdd_tag_t {}; // read and write bdd does not exist.
//---//----------------------------------------------------------------------------------------
/* return some short text for each bdd in `store -b` */
template<>
inline std::string store_entry_to_string <cirkit::bdd_function_t> (
  const cirkit::bdd_function_t& bdd ) {
  return boost::str (
    ( boost::format( "%d variables, %d functions, %d nodes" ) % bdd.first.ReadSize() % bdd.second.size() % bdd.first.ReadKeys() )
    );

}


template<>
inline void print_store_entry<cirkit::bdd_function_t>( std::ostream& os,
						       const cirkit::bdd_function_t& bdd )
{
  for ( const auto& f : cirkit::index( bdd.second ) )
  {
    os << "Function " << f.index << std::endl;
    f.value.PrintMinterm();
    os << std::endl;
  }
}

//---show_store_entry<bdd_function_t>::show_store_entry( command& cmd )
//---{
//---  boost::program_options::options_description bdd_options( "BDD options" );
//---
//---  bdd_options.add_options()
//---    ( "add", "Convert BDD to ADD to have no complemented edges" )
//---    ;
//---
//---  cmd.opts.add( bdd_options );
//---}
//---
//---bool show_store_entry<cirkit::bdd_function_t>::operator()( cirkit::bdd_function_t& bdd,
//---                                                   const std::string& dotname,
//---                                                   const command& cmd ) {
//---  using namespace std::placeholders;
//---  auto * fd = fopen( dotname.c_str(), "w" );
//---
//---  if ( cmd.is_set( "add" ) ) {
//---    std::vector<ADD> adds( bdd.second.size() );
//---    boost::transform( bdd.second, adds.begin(), std::bind( &BDD::Add, _1 ) );
//---    bdd.first.DumpDot( adds, 0, 0, fd );
//---  }
//---  else {
//---    bdd.first.DumpDot( bdd.second, 0, 0, fd );
//---  }
//---
//---  fclose( fd );
//---  return true;
//---}

//---//---command::log_opt_t show_store_entry<bdd_function_t>::log() const
//---//---{
//---//---  return boost::none;
//---//---}
//---
//----------------------------------------------------------------------------------------
/* print statistics on `ps -a` */
template<>
inline void print_store_entry_statistics<cirkit::bdd_function_t>( std::ostream& os,
								  const cirkit::bdd_function_t& bdd )
{
  std::vector<double> minterms;
  for ( const auto& f : bdd.second ) {
    minterms.push_back( f.CountMinterm( bdd.first.ReadSize() ) );
  }

  os << "[i] no. of variables: " << bdd.first.ReadSize() << std::endl
     << "[i] no. of nodes:     " << bdd.first.ReadKeys() << std::endl
     << "[i] no. of minterms:  " << cirkit::any_join( minterms, " " ) << std::endl
     << "[i] level sizes:      " << cirkit::any_join( cirkit::level_sizes( bdd.first, bdd.second ), " " ) << std::endl
     << "[i] maximum fanout:   " << cirkit::maximum_fanout( bdd.first, bdd.second ) << std::endl
     << "[i] complement edges: " << cirkit::count_complement_edges( bdd.first, bdd.second ) << std::endl;


  for ( const auto& p : cirkit::index( bdd.second ) ) {
    os << "[i] info for output " << p.index << ":" << std::endl
       << "[i] - path count:               " << p.value.CountPath() << std::endl
       << "[i] - path count (to non-zero): " << Cudd_CountPathsToNonZero( p.value.getNode() ) << std::endl;
  }

  bdd.first.info();
}




//--//----------------------------------------------------------------------------------------
// complicated. Use the dedicated command aig_to_bdd
//--// conversion                                                                 *
//--/* allow conversion from AIG to BDD with `convert --aig_to_bdd` */
//--template<>
//--inline bool store_can_convert<abc::Gia_Man_t*, cirkit::bdd_function_t>()
//--{
//--  return true;
//--}
//--
//--template<>
//--inline cirkit::bdd_function_t store_convert <abc::Gia_Man_t*, cirkit::bdd_function_t> ( abc::Gia_Man_t* const &gia )
//--{
//--  // there should be only 1 CUDD manager throughout.
//--  // if a Cudd manager exists, take that one. Else create a new one.
//--  Cudd *pmgr;
//--  auto& bdds = env->store <cirkit::bdd_function_t>();
//--  if ( bdds.empty() ) {
//--    pmgr = new Cudd();
//--  }
//--  else {
//--    auto curr_mgr = bdds[0];
//--    pmgr = &curr_mgr;
//--  }
//--  
//--  auto mgr = *pmgr;
//--  cirkit::bdd_simulator simulator( mgr );
//--  auto aig = cirkit::gia_to_cirkit (gia);
//--  auto values = cirkit::simulate_aig( aig, simulator );
//--
//--  std::vector<BDD> bdds;
//--
//--  for ( const auto& o : cirkit::aig_info( aig ).outputs )
//--  {
//--    bdds.push_back( values[o.first] );
//--  }
//--
//--  return {mgr, bdds};
//--
//--}
//--
//--
// DONT know how to do this. Current implmentation aig_from_bdd supports only a single output function.
//---/* allow conversion from BDD to AIG with `convert --bdd_to_aig` */
//---template<>
//---inline bool store_can_convert<cirkit::bdd_function_t, abc::Gia_Man_t*>()
//---{
//---  return true;
//---}
//---
//---template<>
//---inline abc::Gia_Man_t* store_convert<cirkit::bdd_function_t, abc::Gia_Man_t*> (cirkit::bdd_function_t const &bdd)
//---{
//---  return {mgr, bdds};
//---
//---}




/******************************************************************************
 * store entry abc::Abc_Ntk_t*                                                *
 * Old ABC Network type                                                       *
 ******************************************************************************/

/* register abc::Abc_Ntk* as store element for Ntks */
template<>
struct store_info<abc::Abc_Ntk_t*> {
  static constexpr const char* key = "ntks"; /* internal key, must be unique for each store */
  static constexpr const char* option = "ntk"; /* long flag for general commands, `store --ntk` */
  static constexpr const char* mnemonic = "n"; /* short flag for general commands,`store -n` */
  static constexpr const char* name   = "NTK";   /* singular name for option descriptions */
  static constexpr const char* name_plural = "NTKs";  /* plural name for option descriptions */
};

//----------------------------------------------------------------------------------------
/* I/O tag to implement `read_ntk` and `write_ntk` */
//struct io_ntk_tag_t {}; // read and write ntk is part of read_verilog/read_blif etc.
//----------------------------------------------------------------------------------------

/* return some short text for each ntk in `store -n` */
template<>
inline std::string store_entry_to_string <abc::Abc_Ntk_t*> ( abc::Abc_Ntk_t* const& ntk ) {
  return boost::str (
    ( boost::format( "%s   i/o = %d/%d   regs = %d" ) % ntk->pName
      % abc::Abc_NtkPiNum(ntk) % abc::Abc_NtkPoNum(ntk) % abc::Abc_NtkLatchNum (ntk) )
    );

}

//----------------------------------------------------------------------------------------
/* print statistics on `ps -n` */
template<>
inline void print_store_entry_statistics<abc::Abc_Ntk_t*>( std::ostream& os,
							   abc::Abc_Ntk_t* const& ntk ) {


  os << "[i] network name: "     << ntk->pName << std::endl
     << "[i] PI / PO :     " << abc::Abc_NtkPiNum(ntk) << " / " << abc::Abc_NtkPoNum(ntk) << std::endl
     << "[i] number of live objects: " << ntk->nObjs << std::endl
     << "[i] maximum number of levels: " << ntk->LevelMax << std::endl
     << "[i] is combinational network:     " << abc::Abc_NtkIsComb(ntk) << std::endl
     << "[i] is network strashed: " << abc::Abc_NtkIsStrash (ntk) << std::endl
     << "[i] is network topologically sorted " << abc::Abc_NtkIsTopo (ntk) << std::endl
    ;

}



/******************************************************************************
 * store entry abc::Wlc_Ntk_t*                                                *
 * World level circuit, (verilog multi-output bits)                           *
 ******************************************************************************/

/* register abc::Wlc_Ntk_t* as store element for WLCs */
template<>
struct store_info<abc::Wlc_Ntk_t*> {
  static constexpr const char* key = "wlcs"; /* internal key, must be unique for each store */
  static constexpr const char* option = "wlc"; /* long flag for general commands, e.g., `store --aig` */
  static constexpr const char* mnemonic = "w"; /* short flag for general commands, e.g., `store -a` */
  static constexpr const char* name   = "WLC";   /* singular name for option descriptions */
  static constexpr const char* name_plural = "WLCs";  /* plural name for option descriptions */
};

//----------------------------------------------------------------------------------------
/* I/O tag to implement `read_wlc` and `write_wlc` */
struct io_wlc_tag_t {};
//----------------------------------------------------------------------------------------
/* return some short text for each wlc in `store -w` */
template<>
inline std::string store_entry_to_string <abc::Wlc_Ntk_t*> ( abc::Wlc_Ntk_t* const& wlc ) {
  return boost::str (
    boost::format( "%s i/o = %d/%d" ) %
    wlc->pName % abc::Wlc_NtkPiNum( wlc ) % abc::Wlc_NtkPoNum( wlc )
    );
}
//----------------------------------------------------------------------------------------
/* print statistics on `ps -a` */
template<>
inline void print_store_entry_statistics <abc::Wlc_Ntk_t*> (
  std::ostream& os, abc::Wlc_Ntk_t* const& wlc ) {
  abc::Wlc_NtkPrintStats( wlc, 0, 0 );
}



}

//------------------------------------------------------------------------------
#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:



