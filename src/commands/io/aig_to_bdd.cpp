// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_to_bdd.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "aig_to_bdd.hpp"

namespace alice
{

aig_to_bdd_command::rules_t aig_to_bdd_command::validity_rules() const {
  return {
    // rules are pairs, first is a predicate and, next is a string when predicate is false.
    // RULE FORMAT::    { [this]() {} , ""  }
    // rule-1. 
    { [this]() {
	const auto& aigs = env->store<abc::Gia_Man_t*>();
	return id < aigs.size();
      }
      , "Id out of range!"  },

      
    { [this]() {
	const auto& aigs = env->store<abc::Gia_Man_t*>();
	return !aigs.empty();
      }
      , "No AIG in the store!"  },

    
    { [this]() {
	const auto& aigs = env->store<abc::Gia_Man_t*>();
	return (aigs[id])->nRegs == 0 ;
      }
      , "Sequential circuit cannot be converted to BDD!"  }

      
      };
}

bool aig_to_bdd_command::execute() {
  auto& aigs = env->store <abc::Gia_Man_t *>();
  auto gia = aigs[id];
  assert ( gia->nRegs == 0 ); // double check.
  auto& bdds = env->store <cirkit::bdd_function_t>();

  // check aig_to_nkt.cpp for notes on gia to ntk conversion.
  char tmpa[] = "/tmp/blah.aig";
  abc::Gia_AigerWrite ( gia, tmpa, 1, 0 );    
  auto ntk = abc::Io_ReadAiger ( tmpa, 1 );
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  ntk = abc::Abc_NtkToNetlist (ntk); // Convert to Netlist form.
  
  char tmpb[] = "/tmp/blah.blif";
  //auto ntk = abc::Gia_ManToAig ( gia, 0 ); This also crashes!
  abc::Io_WriteBlif (ntk, tmpb, 0, 0, 0);
  Cudd mgr;
  if ( !bdds.empty() ) mgr = bdds[0].first;
  auto bddf = axekit::read_nanotrav_blif ( std::string (tmpb), mgr);
  if ( !bdds.empty() ) {
    std::cout << "[w] All inputs common to all BDDs. (Unpredictable behavior otherwise)\n";
  }
  if ( is_set("new") || bdds.empty() ) {
    bdds.extend();
  } 
  bdds.current() = bddf;
  return true;
  
  //---// Cirkit routine has a rare bug.
  //---// See Bug-1 at the end of this file.
  //---Cudd mgr;
  //---if ( !bdds.empty() ) {
  //---  std::cout << "[w] All inputs common to all BDDs. (Unpredictable behavior otherwise)\n";
  //---}
  //---if ( !bdds.empty() ) mgr = bdds[0].first;
  //---if ( is_set("new") || bdds.empty() )  bdds.extend();
  //---
  //---cirkit::bdd_simulator simulator( mgr );
  //---auto aig = cirkit::gia_to_cirkit (gia);
  //---auto values = cirkit::simulate_aig( aig, simulator );
  //---std::vector<BDD> fs;
  //---for ( const auto& o : cirkit::aig_info( aig ).outputs ) {
  //---  fs.push_back( values[o.first] );
  //---}
  //---bdds.current() = {mgr, fs};
  //---bdd_store_size = bdds.size();
  //---return true;
}

aig_to_bdd_command::log_opt_t aig_to_bdd_command::log() const  {
  return log_opt_t (
    {
      {"aig_id", id},
      {"bdd_id", bdd_store_size - 1}
      }
    );
}


} // namespace alice


/*
Bug-1

WRONG BEHAVIOUR
aXc> read_blif -n -f ./gia-er/ckt5.v.blif 
aXc> read_blif -n -f ./gia-er/ckt6.v.blif 
aXc> aig_to_bdd --id 0 -n
aXc> aig_to_bdd --id 1 -n
aXc> store -b
[i] BDDs in store:
     0: 1 variables, 3 functions, 5 nodes  // Correct ans: 3 variables, 3 functions, 7 nodes
  *  1: 1 variables, 3 functions, 5 nodes  // Correct ans: 3 variables, 3 functions, 7 nodes
------------------------------------------------
ckt5.v
module ckt ( input in1, input in2, input in3, output out1, output out2, output out3  );

   assign out1 = !in1;
   assign out2 = in1;
   assign out3 = !in1;
endmodule

ckt6.v
module ckt ( input in1, input in2, input in3, output out1, output out2, output out3  );
   assign out1 = !in1;
   assign out2 = 0;
   assign out3 = !in1;
endmodule // ckt
*/



//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
