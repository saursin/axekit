// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_to_bdd.hpp
// @brief  : converts an AIG store (in Gia_Man_t*) to a BDD store (bdd_function_t)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef AIG_TO_BDD_HPP
#define AIG_TO_BDD_HPP

#include <commands/axekit_stores.hpp>
#include <utils/common_utils.hpp>
#include <utils/nanotrav/blif_nanotrav_io.hpp>

namespace alice {

class aig_to_bdd_command : public command {
public:
  aig_to_bdd_command (const environment::ptr &env) : command (env, "Convert AIG to BDD")
  {
    opts.add_options()
      ( "id", po::value( &id )->default_value( id ), "store id of AIG circuit" )
      ( "new,n" ,                                  "Create a new BDD store entry" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  unsigned id;
  int bdd_store_size;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
