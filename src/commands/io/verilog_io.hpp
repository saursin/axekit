// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : verilog_io.hpp
// @brief  : read and write verilog (multi-bit output, uses Yosys interface)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef VERILOG_IO_HPP
#define VERILOG_IO_HPP

#include <fstream>
#include <string>
#include <boost/regex.hpp>
#include <commands/axekit_stores.hpp>
#include <utils/common_utils.hpp>

namespace alice {

class read_verilog_command : public command {
public:
  read_verilog_command (const environment::ptr &env) : command (env, "Read Verilog format and store in Aig")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f",   po::value (&file),         "The input Verilog file" )
      ( "debug_log,l",  po::value( &debug_log )->default_value( debug_log ), "Debug the Yosys front-end" )
      ( "top_module,t", po::value (&top_module),   "The top module to be parsed and elaborated" )
      ( "store,s",      po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / Ntk=2 (Old style))" )
      ( "new,n" ,                                  "Create a new store entry" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file;
  std::string debug_log = "NA";
  std::string top_module = "123_"; // * cannot be a valid module name.
  int store = 0;
  float t_read = -1;
  
};

}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
