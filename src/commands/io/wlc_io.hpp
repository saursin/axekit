// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : wlc_io.cpp
// @brief  : read and write wlc (multi-bit output, also called WLC)
//
//------------------------------------------------------------------------------
#pragma once
#ifndef WLC_IO_HPP
#define WLC_IO_HPP

#include <commands/axekit_stores.hpp>

namespace alice {
//----------------------------------------------------------------------------------------
/* enable `read_wlc` for WLCs */
template<>
inline bool store_can_read_io_type <abc::Wlc_Ntk_t*, io_wlc_tag_t> ( command& cmd ) {
  return true;
}

/* implement `read_wlc` for WLCs */
template<>
inline abc::Wlc_Ntk_t* store_read_io_type<abc::Wlc_Ntk_t*, io_wlc_tag_t> (
  const std::string& filename, const command& cmd ) {
  return abc::Wlc_ReadVer ( (char*)filename.c_str() );
}

//----------------------------------------------------------------------------------------
/* enable `write_wlc` for WLCs */
template<>
inline bool store_can_write_io_type <abc::Wlc_Ntk_t*, io_wlc_tag_t> ( command& cmd ) {
  return true;
}

/* implement `write_wlc` for WLCs */
template<>
inline void store_write_io_type <abc::Wlc_Ntk_t*, io_wlc_tag_t> (
  abc::Wlc_Ntk_t* const& wlc, const std::string& filename, const command& cmd ) {
  // use the opts here.
  abc::Wlc_WriteVer ( wlc, (char*)filename.c_str(), 0, 0 );
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
// stores/classical/
// store_can_convert () function
// store_can_convert (from, to) function return boolean
// store_convert () {}


/******************************************************************************
 * conversion                                                                 *
 ******************************************************************************/

// conversion form WLC to normal AIG(all outputs 1 bit) with the command convert --wlc_to_aig
template <>
inline bool store_can_convert < abc::Wlc_Ntk_t*, abc::Gia_Man_t * > () {
  return true;
}

template <>
abc::Gia_Man_t* store_convert <abc::Wlc_Ntk_t*, abc::Gia_Man_t*> ( abc::Wlc_Ntk_t* const &wlc )
{
  return abc::Wlc_NtkBitBlast (wlc, nullptr, 0, 0);
}


//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
