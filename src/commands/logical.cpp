// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : logical.cpp
// @brief  : AND, OR, XOR and NOT of two functions
//           
//           
//------------------------------------------------------------------------------

#include "logical.hpp"

namespace alice
{

logical_command::rules_t logical_command::validity_rules() const {

  return {
    { [this]() {
	return store == 0u;
      }, "Only BDD supported currently. Use store = 0" },

      
    { [this]() {
	if (0u == store) { // BDD store
	  const auto& bdds = env->store<cirkit::bdd_function_t>();
	  if ( is_set("abs") )
	    return id1 < bdds.size();
	  else
	    return id1 < bdds.size() && id2 < bdds.size();
	}
	else { // AIG store
	  return false;
	}
      }, "Store ids are out of range" },

      
    { [this]() {
	return is_set("add") | is_set("sub") | is_set("abs") ;
      }, "Specify which function to compute." },

      
    { [this]() {
	return  !(
	  ( is_set("and") & is_set("or")  ) |
	  ( is_set("and") & is_set("xor") ) |
	  ( is_set("and") & is_set("not") ) | 
	  ( is_set("or")  & is_set("xor") ) |
	  ( is_set("or")  & is_set("not") ) |
	  ( is_set("xor") & is_set("not") )

	  )
	  ;
      }, "Only one function can be computed at a time." }
    
  };

}

bool logical_command::execute() {
  if (0u == store) {
    auto& bdds = env->store<cirkit::bdd_function_t> ();
    auto fs = bdds[id1];
    cirkit::bdd_function_t fshat;
    cirkit::bdd_function_t result;
    
    if ( is_set ("and") )  {
      fshat = bdds[id2];
      result = and_bdd (fs, fshat);
    }

    if ( is_set ("or") )  {
      fshat = bdds[id2];
      result = or_bdd (fs, fshat);
    }

    if ( is_set ("xor") )  {
      fshat = bdds[id2];
      result = xor_bdd (fs, fshat);
    }

    if ( is_set ("not") )  {
      result = not_bdd (fs);
    }


    if ( is_set("new") || bdds.empty() ) {
      bdds.extend();
    }
    if ( result.second.empty() ) std::cout << "[w] Result is empty BDD vector!\n";
    bdds.current() = result;

  }

  else {
    assert (false);
    auto& aigs = env->store<abc::Gia_Man_t*> ();
    auto fs = aigs[id1];
    auto fshat = aigs[id2];
    

    if ( is_set("new") || aigs.empty() ) {
      aigs.extend();
    } 
    //aigs.current() = gia;
  }

  
  return true;
}



logical_command::log_opt_t logical_command::log() const  {
  return log_opt_t (
    {
      {"function", is_set("and") ? std::string("and") :
	  ( is_set("or") ? std::string("or") :
	    ( is_set ("xor") ? std::string("xor") : std::string("not") ) 
	    ) },
      {"store_used", (store == 0u) ? std::string("BDD") : std::string("AIG") },
      {"store_id1", id1 },
      {"store_id2", id2 }
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
