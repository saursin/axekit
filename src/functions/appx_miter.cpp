// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : appx_miter.cpp
// @brief  : functions to create and solve appx miters.
//------------------------------------------------------------------------------

#include "appx_miter.hpp"

namespace axekit
{
//------------------------------------------------------------------------------
// some variables to hold local data
namespace {
std::string work_path = "./work";
unsigned debug = 1;
std::string orig_vlog;
bool is_initialized = false;
}

// initialize the approximation miter common functions.
// should be done once in the program in the beginning.
void init_appx_miter (const std::string &dir_work_path, const unsigned &debug_val,
		      const std::string &orig_verilog) {
  work_path = dir_work_path;
  debug = debug_val;
  orig_vlog = orig_verilog;

  boost::filesystem::path p(work_path);
  if ( ! boost::filesystem::exists (p) ) {
    std::cout << "[w] Directory \"" + work_path + "\" does not exist. "
	      << "Will be created for Approx Miter purpose"  << std::endl;
    auto status = boost::filesystem::create_directory (p);
    if (!status)  std::cout << "[e] Cannot create work directory "
			    << "(Permissions? Disk full?)" << std::endl;
    assert (status);
  } 
  is_initialized = true;
}

inline void print_time (std::string msg) { // just avoid unnecessary code clutter
  if (debug > 10) print_dbg (msg + std::string( curr_time()) );
}


SolverResult solve_wc_appx_miter (const Design &design, const Port &port_to_check, 
				  const unsigned &limit, Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_wc.v";
  std::string appx_vlog = work_path + "appx.v";

  write_worst_case_miter (design, port_to_check, "wc_miter", miter_vlog, limit); 
  print_time ("2. wrote worst case miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");
  
  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "wc_miter" );
  print_time ("4. miter vlog to blif  : ");

  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;

}


SolverResult solve_bf_appx_miter (const Design &design, const Port &port_to_check, 
				  const unsigned &limit, Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );  
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_bf.v";
  std::string appx_vlog = work_path + "appx.v";

  write_bit_flip_miter (design, port_to_check, "bf_miter", miter_vlog, limit);
  print_time ("2. wrote bit flip miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");

  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "bf_miter" );
  print_time ("4. miter vlog to blif  : ");
  

  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;

}


// This will return if its >= . All other cases return only > 
SolverResult solve_ge_bf_appx_miter (const Design &design, const Port &port_to_check, 
				  const unsigned &limit, Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );  
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_bf.v";
  std::string appx_vlog = work_path + "appx.v";
  
  // ge -> greater than or equal to miter.
  write_ge_bit_flip_miter (design, port_to_check, "bf_miter", miter_vlog, limit);
  print_time ("2. wrote bit flip miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");

  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "bf_miter" );
  print_time ("4. miter vlog to blif  : ");
  

  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;

}


SolverResult solve_wc_bf_appx_miter (const Design &design, const Port &port_to_check, 
				     const unsigned &wc_limit, const unsigned &bf_limit, 
				     Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );
  start_yosys();
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_wc_bf.v";
  std::string appx_vlog = work_path + "appx.v";

  write_wc_bf_miter (design, port_to_check, "wc_bf_miter", miter_vlog, wc_limit, bf_limit);
  print_time ("2. wrote worst-case + bit-flip miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");

  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "wc_bf_miter" );
  print_time ("4. miter vlog to blif  : ");
  
  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;


}  





//###########################################################################
//###########################################################################
//###########################################################################
// TEMPORARY VERSIONS FOR AxC in TCAD.


SolverResult solve_wc_appx_miter (const Design &design, const unsigned &limit, Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_wc.v";
  std::string appx_vlog = work_path + "appx.v";

  write_worst_case_miter (design, "wc_miter", miter_vlog, limit); 
  print_time ("2. wrote worst case miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");
  
  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "wc_miter" );
  print_time ("4. miter vlog to blif  : ");

  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;

}


SolverResult solve_bf_appx_miter (const Design &design, const unsigned &limit, Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );  
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_bf.v";
  std::string appx_vlog = work_path + "appx.v";

  write_bit_flip_miter (design, "bf_miter", miter_vlog, limit);
  print_time ("2. wrote bit flip miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");

  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "bf_miter" );
  print_time ("4. miter vlog to blif  : ");
  

  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;

}


SolverResult solve_wc_bf_appx_miter (const Design &design,
				     const unsigned &wc_limit, const unsigned &bf_limit, 
				     Network work_ntk) {
  assert ( is_initialized  && "where is init_appx_miter()?" );

  start_yosys();
  // solve the miter. write out the miter logic up.
  auto miter_vlog = work_path + "miter_wc_bf.v";
  std::string appx_vlog = work_path + "appx.v";

  write_wc_bf_miter (design, "wc_bf_miter", miter_vlog, wc_limit, bf_limit);
  print_time ("2. wrote worst-case + bit-flip miter  : ");
  auto mblif = work_path + "miter_tmp.blif";
  abc_ntk_inline::write_verilog_from_network (appx_vlog, work_ntk);
  print_time ("3. wrote verilog from network  : ");
  
  cat_three_files ( miter_vlog, orig_vlog, appx_vlog, miter_vlog ); // miter = orig + appx + miter
  verilog_to_blif_in_ys ( mblif, miter_vlog, "wc_bf_miter" );

  print_time ("4. miter vlog to blif  : ");
  
  auto miter_ntk = abc_ntk_inline::read_blif_to_network (mblif);
  print_time ("5. read blif to network  : ");
  auto results = has_limit_crossed (miter_ntk);
  print_time ("6. has limit crossed  : ");
  abc_ntk_inline::delete_network ( miter_ntk ); // recycle these networks
  return results;


}  




//------------------------------------------------------------------------------
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
