// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : bdd_error_metrics.hpp
// @brief  : functions to report error metrics using bdd.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef BDD_ERROR_METRICS_HPP
#define BDD_ERROR_METRICS_HPP

#include <cirkit/cudd_arithmetic.hpp>
#include <functions/characteristic.hpp>
#include <functions/bdd/bdd_logical.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

namespace axekit {

boost::multiprecision::uint256_t error_rate( const bdd_function_t& f,
					     const bdd_function_t& fhat);

boost::multiprecision::uint256_t worst_case ( const bdd_function_t &fs,
					      const bdd_function_t &fshat );

boost::multiprecision::uint256_t worst_case_chi ( const bdd_function_t &fs,
						  const bdd_function_t &fshat );

boost::multiprecision::cpp_dec_float_100 average_case ( const bdd_function_t &fs,
							const bdd_function_t &fshat );

boost::multiprecision::cpp_dec_float_100 sum_of_errors ( const bdd_function_t &diff );

boost::multiprecision::uint256_t bit_flip_error ( const bdd_function_t &fs,
						  const bdd_function_t &fshat );

boost::multiprecision::uint256_t bit_flip_error_chi ( const bdd_function_t &fs,
						      const bdd_function_t &fshat );


}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
