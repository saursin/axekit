// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_first_cut.hpp
// @brief  : Take the first cut in the longest path
//           Purpose : do some trivial approximation for verification.
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef AIG_FIRST_CUT_HPP
#define AIG_FIRST_CUT_HPP

#include <cirkit/bdd_utils.hpp>
#include <iostream>
#include <tuple>
#include <fstream>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <algorithm>
#include <iomanip>

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <utils/program_options.hpp>
#include <utils/common_utils.hpp>
#include <utils/yosys_utils.hpp>
#include <utils/miter_utils.hpp>
#include <utils/abc_utils.hpp>

#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>

#include <cirkit/cudd_cirkit.hpp>

#include <functions/has_limit_crossed.hpp>
#include <functions/path_routines.hpp>
#include <functions/cut_routines.hpp>
#include <functions/appx_miter.hpp>
#include <functions/cirkit_routines.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>


namespace axekit {

using namespace cirkit;

Network aig_first_cut ( Network ntk );
std::tuple <float, float, float>  from_aig_first_cut ();

}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
