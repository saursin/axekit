// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_rewrite.cpp
// @brief  : Approximation Rewriting in AIG (ICCAD-2016)
//
// TODO : rewrite entire algorithm properly.
//
//        Reads in a Network (Abc_Aig type not Gia), writes out blif.
//        this way completey seperated from AxC store. Read into the AxC store
//        from this blif as a separate independent step.
//        This is a premature integration, only for TCAD. Hence should not corrupt
//        original store.
//------------------------------------------------------------------------------

#include "aig_rewrite.hpp"


namespace axekit
{

using namespace cirkit;

//------------------------------------------------------------------------------
using mpi = boost::multiprecision::int256_t;
using mpf = boost::multiprecision::cpp_dec_float_100;

namespace // Some of these should/may come from the compile command.
{ 

auto debug = 0;          // debug level for printing messages
auto max_cut_size = 3;   // constant for one run. To experiment with different cut-sizes.
auto max_attempts = 10u; // Maximum cut attempts allowed.
auto applied_cuts = 0u;  // Tracks number of succesful cuts for approx.
int effort = 3;


int wc_limit = -1;
int ac_limit = -1;
int bf_limit = -1;
mpf er_limit = -1;


bool wc_flag = false;
bool ac_flag = false;
bool bf_flag = false;
bool er_flag = false;
bool wc_bf_flag = false;

int opt_style = 0; // default delay

// for error rate computations.
cirkit::bdd_function_t golden_bdd;
Cudd *cudd_mgr;
boost::multiprecision::uint256_t target_error_count(0u);
std::string oblif;


float t_time_sort_paths = 0u, t_time_apply_cut = 0u, t_time_generate_cuts = 0u;

}


// ToDo: complete rewrite. get away with double conversions, unnecessary type changes etc.
void to_aig_rewrite1 (mpf er_frac, mpi wc, mpf ac, mpi bf,
		     bool wc__flag, bool ac__flag, bool er__flag, bool wc__bf__flag ) {

  er_limit = er_frac;
  wc_limit = static_cast<int> (wc); ac_limit = static_cast<int> (ac), bf_limit = static_cast<int> (bf);
  wc_flag = wc__flag; ac_flag = wc__flag; er_flag = er__flag; wc_bf_flag = wc__bf__flag;
  
}
void to_aig_rewrite2 (int debug__in, int max__cut__size, int max__attempts, int effort__in) {
  debug = debug__in; max_cut_size = max__cut__size;
  max_attempts = max__attempts; effort = effort__in;
}


Network aig_rewrite (Network ntk) {
  assert ( abc_ntk_inline::is_network_good(ntk) );
  // Note: anyway we lose the network name, port-name etc.
  // For now, no need to waste time on annotating these.
  abc_ntk_inline::set_network_name (ntk, "dummy");
  std::srand ( unsigned  (std::time(0)) );  
  const std::string work = ".aig_rewrite_work_" +  boost::lexical_cast<std::string> (rand());
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; 
  oblif = work_path + "appx_output_" + boost::lexical_cast<std::string> (rand()) + ".blif";
  auto design = abc_ntk_inline::make_design_from_network (ntk);
  
  std::string orig_vlog = work_path + "orig.v";
  if ( !abc_ntk_inline::check_network (ntk) )
    std::cout << "[e] Error. Original Network integrity failed..  :("
	      << std::endl;
  abc_ntk_inline::write_verilog_from_network (orig_vlog, ntk);
  auto orig_ntk_name = abc_ntk_inline::get_network_name ( ntk );
  auto orig_ntk_node_cnt = abc_ntk_inline::get_node_count ( ntk );
  
  //-----------------------------------------------------------
  init_appx_miter (work_path, debug, orig_vlog); // initialize  Approx-Miter
  //-----------------------------------------------------------
  cudd_mgr = new Cudd();
  if (er_flag) { // get the golden BDD for error-rate
      
    boost::multiprecision::uint256_t one(1u);
    target_error_count  = ( one << (abc_ntk_inline::get_pi_list (ntk)).size() ) * (unsigned)er_limit ;
    target_error_count = target_error_count / 100;
    golden_bdd = network_to_bdd (ntk, *cudd_mgr);
  }
  //------------------------------------------------------------
  auto attempts = 0u;

  auto appx_ntk = abc_ntk_inline::copy_network ( ntk ); 
  abc_ntk_inline::set_network_name ( appx_ntk,
				     abc_ntk_inline::get_network_name (ntk) + "_appx" );
  //std::vector <Object> tried_node_list; // keep track of tried nodes.
  // Storing pointers in vector is a bug.
  std::vector <std::string> tried_node_list; // keep track of tried nodes.
    
  while (attempts < max_attempts) // The Main approximation Loop.
  {
    if (1 == attempts && 0 == applied_cuts) { // hopeless, not going to improve.
      std::cout << "[w] Stopping approximation here. Cant find any suitable cut\n";
      std::cout << "[w] Approximated and Original are same networks!\n";
      break;
    }
    attempts++;
    appx_ntk = abc_ntk_inline::strash_network (appx_ntk); // Re-strash 
    assert ( abc_ntk_inline::is_topologically_sorted (appx_ntk) );
    // 3. Sort the longest paths.
    auto begin_time = std::clock ();
    auto po_list = abc_ntk_inline::get_po_list (appx_ntk);
    auto nTop = (unsigned)po_list.size();  // sort everything. (default max-effort)
    if (2 == effort) nTop = std::min (10u, nTop);
    if (1 == effort) nTop = std::min (5u,  nTop);
    if (0 == effort) nTop = std::min (1u,  nTop);
    std::vector <Path> paths_list;
    for (auto &po : po_list) { // TODO: interested only in nTop paths.
      auto longest_path = get_longest_path_to_po (po);
      // prune const drivers to PO
      if ( abc_ntk_inline::is_node_const ( get_pi_of_path (longest_path) ) ) continue;
      paths_list.emplace_back (longest_path);
    }
    if ( 0u == po_list.size()) break; // case where all PO drivers are consts
    sort_paths ( paths_list, nTop);

    auto time_sort_paths = get_elapsed_time ( begin_time );
    t_time_sort_paths += time_sort_paths; // total time
    //------------------------------------------------------------
    // Merge same length paths first. 
    // This is a distructive call, paths_list will be useless except only for 
    // our algorithm.
    // And there will be duplications in nodes. (use tried_list_nodes for this)
    if (0 == opt_style) {
      merge_same_length_paths (paths_list);
      nTop = (unsigned)paths_list.size();  // after merging size will change.
    }
    //------------------------------------------------------------
    // CUTS 
    // Get a flattend list of nodes to cut based on 1) longest path & 2) cut-volume.
    // 4. Sort based on cut-volume
    if (debug > 1) std::cout << "[i] Cut generation begin :: " << curr_time();
    begin_time = std::clock();
    auto cut_mgr = abc_ntk_inline::start_cut_manager (max_cut_size);
    auto total_nodes = abc_ntk_inline::get_node_count ( appx_ntk );
    std::vector <Object> node_list;
    for (auto i = 0u; i < nTop; i++) {
      auto cut_volume_list = get_cut_volume_list ( cut_mgr, paths_list[i] );
      if (0 == opt_style)  // default delay
	reverse_sort_cut_volume_list (cut_volume_list);
      else if (1 == opt_style) // area 
	sort_cut_volume_list ( cut_volume_list );
      else  // pick random cuts.
	randomize_cut_volume_list ( cut_volume_list );
      std::transform ( cut_volume_list.begin(), cut_volume_list.end(),
		       back_inserter(node_list),
		       [] (const CutVolume &cut) {return cut.first;} );
    }
    if (debug > 1) std::cout << "[i] Cut generation end :: " << curr_time();
    auto time_generate_cuts = get_elapsed_time ( begin_time );
    t_time_generate_cuts += time_generate_cuts; // total time

    // 6. Do the cutting.
  
    if (debug > 1) std::cout << "[i] Apply Cut & Solve begin :: " << curr_time();
    begin_time = std::clock();
    std::string cut_node_name;
    auto applied_cuts_before = applied_cuts;
    for (auto &node : node_list) {
      //assert (node_ctr < 4); node_ctr++;
      auto curr_node_name = std::string (abc_ntk_inline::get_obj_name(node));
      auto is_node_tried = std::find ( tried_node_list.begin(), tried_node_list.end(), 
				       curr_node_name );
      if ( is_node_tried != tried_node_list.end() ) continue;
      tried_node_list.emplace_back (curr_node_name);
      if ( !abc_ntk_inline::is_node_good(node) ) continue;
      if ( !abc_ntk_inline::is_node_internal(node) ) continue;
      auto work_ntk = abc_ntk_inline::copy_network ( appx_ntk ); // Try first on a work copy.
      // two different networks, this translation is a must.
      auto node_work = abc_ntk_inline::get_ith_object ( work_ntk, node->Id );
      if ( node_work == nullptr ) continue; // TODO : remove leak
      abc_ntk_inline::replace_node1_with_node2 (work_ntk, node_work,  abc_ntk_inline::get_const1 (work_ntk));
      // create and solve the approximation miters.
      SolverResult results = SolverResult::UNINIT;
      // One and only of these flags must be true.
  
      if (wc_flag) results = solve_wc_appx_miter (design, wc_limit, work_ntk);
      if (bf_flag) results = solve_bf_appx_miter (design, bf_limit, work_ntk);
      
      if (wc_bf_flag) results = solve_wc_bf_appx_miter (design, wc_limit, bf_limit, work_ntk);
      //return;
  
      //------------------------------------------------------------------------
      // case 1, only err
      if ( results == SolverResult::UNINIT ) { // no wc and bf
	auto approx_bdd = network_to_bdd (work_ntk, *cudd_mgr);
	auto error_count = axekit::error_rate (golden_bdd, approx_bdd);
	abc_ntk_inline::delete_network ( work_ntk );  // dont need anymore
	
	if (error_count < target_error_count) {
	  // Replay and there is further scope for cutting.
	  if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				   <<  " - successful, keeping the current cut [err = " 
				   << error_count << "]\n";
	  //cut_node_name = get_obj_name (node); // after update names change, hence meaningless.
	  abc_ntk_inline::replace_node1_with_node2 ( appx_ntk, node, abc_ntk_inline::get_const1 (appx_ntk) );
	  applied_cuts++;
	  break; // Need to recompute everything. After replacement, nothing holds.
	}
	else {
	  if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				   <<  " - unsuccessful, skipping the current cut\n";
	}
      } // case 1 ends
      // case 2 wc/bf/er combinations.
      else {
	if ( results == SolverResult::NO ) {
	  if (er_flag) { // 2.1. wc/bf + er
	    auto approx_bdd = network_to_bdd (work_ntk, *cudd_mgr);
	    auto error_count = axekit::error_rate (golden_bdd, approx_bdd);
	    abc_ntk_inline::delete_network ( work_ntk );  // dont need anymore
	    if (error_count < target_error_count) {
	      // Replay and there is further scope for cutting.
	      if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				       <<  " - successful, keeping the current cut [er = " 
				       << error_count << "]\n";
	      abc_ntk_inline::replace_node1_with_node2 ( appx_ntk, node, abc_ntk_inline::get_const1 (appx_ntk) );
	      applied_cuts++;
	      break; // Need to recompute everything. After replacement, nix holds.
	    }
	    else {
	      if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				       <<  " - unsuccessful, skipping the current cut\n";
	    }
	  } 
	  else { // 2.2. only wc/bf
	    abc_ntk_inline::delete_network ( work_ntk );  // dont need anymore
	    // Replay and there is further scope for cutting.
	    if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				     <<  " - successful, keeping the current cut\n";
	    //cut_node_name = get_obj_name (node); // after update names change, hence meaningless.
	    abc_ntk_inline::replace_node1_with_node2 ( appx_ntk, node, abc_ntk_inline::get_const1 (appx_ntk) );
	    applied_cuts++;
	    break; // Need to recompute everything. After replacement, nix holds.
	  }
	} // ! results == SolverResult::NO
	else { // 2.3. All unsuccessful. Proceed with nxt iteration.
	  abc_ntk_inline::delete_network ( work_ntk );  // dont need anymore
	  if (debug > 1) std::cout << "[i] Cutting Node " << abc_ntk_inline::get_obj_name (node)
				   <<  " - unsuccessful, skipping the current cut\n";
	}
      } // case 2 ends
      //------------------------------------------------------------------------
    } // for (auto &node : node_list)
    
    auto time_apply_cut = get_elapsed_time ( begin_time );
    t_time_apply_cut += time_apply_cut; // total time
    if ( applied_cuts_before == applied_cuts ) { // not going to improve.
      if (debug > 1)  std::cout << "[i] Stopping approximation. Cannot improve further.\n";
      break;
    }
  } // while (attempts < max_attempts)

  if ( !abc_ntk_inline::check_network (appx_ntk) )
    std::cout << "[e] Error. Approximated Network integrity failed..  :(" << std::endl;

  return appx_ntk;
  
}


// return the output appx-network name, and all the timings.
std::tuple <float, float, float>  from_aig_rewrite () {
  
  return std::make_tuple (
    t_time_sort_paths, t_time_apply_cut, t_time_generate_cuts
    );

}

//------------------------------------------------------------------------------
} // namespace axekit





// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
