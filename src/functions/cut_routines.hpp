// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cut_routines.hpp
// @brief  : various routines for cut-set evaluation
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef CUT_ROUTINES_HPP
#define CUT_ROUTINES_HPP

#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <ext-libs/abc/abc_api.hpp>
#include <types/Path.hpp>
#include <types/AbcTypes.hpp>
#include <types/AppxSynthesisTypes.hpp>
#include <utils/abc_utils.hpp>

namespace axekit {

int get_volume_of_cut ( Object &node, ObjVector &leaves );
std::vector <CutVolume> get_cut_volume_list ( CutManager &cut_mgr, Path &path );
void sort_cut_volume_list ( std::vector <CutVolume> &cut_volume_list );
void print_cut_volume_list ( CutManager &cut_mgr, Path &path, std::ofstream &rpt, 
			     const unsigned &debug );
void reverse_sort_cut_volume_list ( std::vector <CutVolume> &cut_volume_list );
void randomize_cut_volume_list ( std::vector <CutVolume> &cut_volume_list );
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
