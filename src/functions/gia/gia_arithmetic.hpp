// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_arithmetic.cpp
// @brief  : Arithemtic functions using Gia
//------------------------------------------------------------------------------

#pragma once
#ifndef GIA_ARITHMETIC_HPP
#define GIA_ARITHMETIC_HPP

#include <unordered_map>
#include <vector>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>

#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <cassert>
#include <stack>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>
#include <cirkit/bitset_utils.hpp>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <sat/cnf/cnf.h>
#include <sat/bsat/satSolver.h>
#include <ext-libs/abc/abc_api.hpp>

#include <functions/gia/gia_lexsat.hpp>

#define LN( x ) if ( verbose ) { std::cout << x << std::endl; }


namespace axekit
{

using namespace cirkit;

// returns the absolute value of difference. i.e. |f-fhat|
abc::Gia_Man_t * subtract_gia ( abc::Gia_Man_t *f, abc::Gia_Man_t *fhat );

// lits-sum, lit-carry
std::pair < std::vector<int>, int > full_adder ( abc::Gia_Man_t *gia, const std::vector<int> &x,
						 const std::vector<int> &y, int cin );

boost::multiprecision::uint256_t get_max_value ( abc::Gia_Man_t *gia );
//---boost::multiprecision::uint256_t get_weighted_sum ( abc::Gia_Man_t *gia );
abc::Gia_Man_t * add_individual_outputs (abc::Gia_Man_t *pOne);

}
#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
