// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_logical.hpp
// @brief  : Logical functions to operate on multi-output Gia.
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef GIA_LOGICAL_HPP
#define GIA_LOGICAL_HPP

#include <vector>
#include <cassert>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <ext-libs/abc/abc_api.hpp>


namespace axekit {

abc::Gia_Man_t * xor_gia (abc::Gia_Man_t *x, abc::Gia_Man_t *y);
abc::Gia_Man_t * and_gia (abc::Gia_Man_t *x, abc::Gia_Man_t *y);
abc::Gia_Man_t * or_gia  (abc::Gia_Man_t *x, abc::Gia_Man_t *y);
abc::Gia_Man_t * not_gia (abc::Gia_Man_t *gia);

abc::Gia_Man_t * xor_individual_outputs (abc::Gia_Man_t *x);
abc::Gia_Man_t * and_individual_outputs (abc::Gia_Man_t *x);
abc::Gia_Man_t * or_individual_outputs  (abc::Gia_Man_t *x);

}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
