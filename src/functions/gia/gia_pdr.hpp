// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_pdr.hpp
// @brief  : Property directed reachability
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef GIA_PDR_HPP
#define GIA_PDR_HPP

#include <vector>
#include <utility>
#include <iostream>
#include <cassert>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <ext-libs/abc/abc_api.hpp>


namespace axekit {

bool pdr ( abc::Abc_Ntk_t *pNtk );
bool pdr ( abc::Gia_Man_t *gia );

}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
