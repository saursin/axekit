// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : has_limit_crossed.cpp
// @brief  : creates the miter and checks if it crosses the limit.
//------------------------------------------------------------------------------

#include "has_limit_crossed.hpp"

namespace axekit
{
// Just a wrapper to interpret the solver results.
SolverResult has_limit_crossed ( Network &miter_ntk )
{
  auto result = prove_miter ( miter_ntk );
  return ProveResult_to_SolverResult ( result );
}

//------------------------------------------------------------------------------
abc::ProveResult prove_miter ( Network &miter_ntk ) {
  assert ( abc_ntk_inline::is_network_good (miter_ntk) );
  assert ( abc_ntk_inline::is_network_strashed (miter_ntk) && "[e] Error Network not strashed!" );
  ProveParameters params;
  auto results = abc::Abc_NtkIvyProve ( &miter_ntk, &params );
  if (-1 == results) return abc::ProveResult::UNDECIDED; // Time Out, resource limit.
  if (0 == results) {
    int * sim_info = abc::Abc_NtkVerifySimulatePattern ( miter_ntk, miter_ntk->pModel );
    if ( sim_info[0] != 1 ) return abc::ProveResult::ERROR; // Counter Example is invalid.
    else return abc::ProveResult::NOT_EQUIVALENT; // Not equivalent.  :-(
    //overcautious. in reality, free() has no problems with nullptr 
    if ( sim_info != nullptr ) free ( (char *) sim_info );  // came with malloc()
  }
  return abc::ProveResult::EQUIVALENT; // Equivalent.   :-)
}
//------------------------------------------------------------------------------


  
//------------------------------------------------------------------------------
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
