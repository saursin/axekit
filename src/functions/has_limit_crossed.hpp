// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : has_limit_crossed.hpp
// @brief  : creates the miter and checks if it crosses the limit.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef HAS_LIMIT_CROSSED_HPP
#define HAS_LIMIT_CROSSED_HPP

#include <iostream>
#include <cassert>

#include <types/AbcTypes.hpp>
#include <types/Design.hpp>
#include <types/AppxSynthesisTypes.hpp>
#include <utils/abc_utils.hpp>
#include <utils/miter_utils.hpp>
#include <utils/yosys_utils.hpp>

namespace axekit {

abc::ProveResult prove_miter ( Network &miter_ntk );
SolverResult has_limit_crossed ( Network &miter_ntk );
  
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
