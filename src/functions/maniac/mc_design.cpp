// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)

/**
 * @file mc_design.hpp
 *
 * @brief Data class to hold the design info
 *
 * @author Arun 
 */

#include <iostream>
#include <fstream>
#include "mc_design.hpp"
#include <algorithm>
#include <utils/common_utils.hpp>

namespace Yosys
{
bool is_yosys_initialized () {return true;}
}

namespace maniac
{

//------------------------------------------------------------------------------
// Return the MSB position of the number.
// NOTE: WORKS ONLY FOR UNSIGNED. FOR SIGNED NEED TO MASK SIGN BIT (32nd or 64th)
unsigned msb_pos_of_unsigned (const unsigned long &num) {
  auto length = sizeof(num) * 8u;
  // it is 0 to N-1, alloting 1 + size-required.
  boost::dynamic_bitset<> num_bs (length + 1, num); 
  for (auto i = length; i > 0u; i--)
    if ( num_bs.test(i) ) return i;
  return 0;
}


char *  curr_time () {
  return axekit::curr_time();
}

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
void mc_design::get_ports (Yosys::RTLIL::Module *module) {
  assert ( Yosys::is_yosys_initialized() );
  //std::vector<RTLIL::Wire*> all_inputs;
  //std::vector<RTLIL::Wire*> all_outputs;

  // "clock" is the default clock name for ABC.
  // If a signal matches the name "clock", ignore.
  // if signal matches wit cli argument clock, then also ignore.
  // if signal matches wit cli argument reset, ignore.
  // if signal matches wit cli argument oe, ignore.
  for (auto &w : module->wires_) { // _wires is a dict in Module
    auto port_name = ((w.second)->name).str();
    port_name = Yosys::proper_name (port_name);

    if (clock == port_name) continue; // Default name is "clock"
    if (reset == port_name) continue; // Default name is "rst"
    if (oe == port_name) continue; // Default is not available "__NA__"
      
    if (w.second->port_input)
    {
      input_ports.push_back ( std::make_pair( port_name, (w.second)->width ) );
    }
    if (w.second->port_output)
    {
      //std::cout << "portname :: " << port_name << "  width "<< (w.second)->width << std::endl;
      output_ports.push_back ( std::make_pair( port_name, (w.second)->width ) );
    }
  }
}
//------------------------------------------------------------------------------

void mc_design::extract_ports () {
  assert ( Yosys::is_yosys_initialized() );
  
  Yosys::run_yosys_cmd("read_verilog  " + filename, localdebug);
  auto design = Yosys::yosys_get_design();
  if (!Yosys::has_module ( top_module, design )) { // make sure module exists.
    std::cout << "[e] " << top_module << " not found in design "
	      << filename << std::endl;
    assert(false);
  }
  Yosys::run_yosys_cmd("hierarchy -check -top " + top_module, localdebug);
  auto curr_top = design->top_module();  // get the top module.
  get_ports (curr_top);
  
  Yosys::run_yosys_cmd("design -reset", localdebug); 
}


/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
mc_design::mc_design( const std::string &vlog_file,
		      const std::string &top_module_name,
		      const std::string &clk, const std::string &rst,
		      const std::string &out_enable, bool debug )
{
  top_module = top_module_name;
  filename = vlog_file;
  localdebug = debug;
  clock = clk; reset = rst; oe = out_enable;
  extract_ports();
}
//------------------------------------------------------------------------------
void mc_design::write ( const std::string &out_filename, const std::string &format ) const
{
  write ( out_filename, top_module, format, 0 );
}
void mc_design::write ( const std::string &out_filename,
			const std::string &format, const unsigned &opt ) const
{
  write ( out_filename, top_module, format, opt );
}
//------------------------------------------------------------------------------
void mc_design::write ( const std::string &out_filename,
			const std::string &new_module, const std::string &format
			) const
{
  write (out_filename, new_module, format, 0);
}
void mc_design::write ( const std::string &out_filename,
			const std::string &new_module, const std::string &format,
			const unsigned &opt) const
{
  assert ( Yosys::is_yosys_initialized() );
  Yosys::run_yosys_cmd("read_verilog  " + filename, localdebug); // re-read the file.
  auto design = Yosys::yosys_get_design();
  if (!Yosys::has_module ( top_module, design )) { // make sure module exists.
    std::cout << "[e] " << top_module << " not found in design "
	      << filename << std::endl;
    assert(false);
  }
  Yosys::run_yosys_cmd("hierarchy -check -top " + top_module, localdebug);
  auto curr_top = design->top_module();  // get the top module.
  
  Yosys::run_yosys_cmd("proc", localdebug);
  if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
  // seg-fault with memory() command. some yosys bug.
  // Problem only wit libyosys, otherwise Ok with the standalone yosys.
  Yosys::run_yosys_cmd("memory_dff", localdebug);
  Yosys::run_yosys_cmd("opt_clean", localdebug);
  //Yosys::run_yosys_cmd("memory_share", localdebug); // Buggy in libyosys.
  Yosys::run_yosys_cmd("opt_clean", localdebug);
  Yosys::run_yosys_cmd("memory_collect", localdebug);
  Yosys::run_yosys_cmd("opt_clean", localdebug);
  Yosys::run_yosys_cmd("memory_map", localdebug);
  Yosys::run_yosys_cmd("opt_clean", localdebug);
  //Yosys::run_yosys_cmd("memory", localdebug); // f/b from our JPEG experiments.

  Yosys::run_yosys_cmd("flatten", localdebug); Yosys::run_yosys_cmd("clean", localdebug);
  // Individually optimize fully before writing out.
  // This will reduce memory and processor usage down the flow.
  Yosys::run_yosys_cmd("fsm", localdebug);
  //Yosys::run_yosys_cmd("techmap", localdebug); - causes problems, but is a must in some cases.
  Yosys::run_yosys_cmd("opt", localdebug);
  Yosys::run_yosys_cmd("flatten", localdebug);
  if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
  Yosys::run_yosys_cmd("clean", localdebug);
  if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
  Yosys::run_yosys_cmd("rename -top " + new_module, localdebug);
  if (format == std::string ("verilog")) {
    if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
    Yosys::run_yosys_cmd("write_verilog " + out_filename, localdebug);
  }
  else if (format == std::string ("blif")) {
    if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
    Yosys::run_yosys_cmd("techmap", localdebug);
    Yosys::run_yosys_cmd("clean", localdebug);
    if(opt == 1u)   Yosys::run_yosys_cmd("opt", localdebug);
    Yosys::run_yosys_cmd("write_blif " + out_filename, localdebug);
  }
  else {
    std::cout << "[e] Unknown format (" << format
	      << ") requested to writeout. Should be either blif or verilog"
	      << std::endl;
    assert (false);
  }
  Yosys::run_yosys_cmd("design -reset", localdebug);
  
}
//------------------------------------------------------------------------------
void mc_design::write_ports ( const std::string &out_filename )
{
  std::ofstream file;
  file.open (out_filename);
  file << "//port-name  width" << std::endl;
  for (auto &p : input_ports) {
    file << p.first << " " << p.second << std::endl;
  }
  for (auto &p : output_ports) {
    file << p.first << " " << p.second << std::endl;
  }
  file.close();
}
//------------------------------------------------------------------------------

unsigned mc_design::get_width ( const std::string &port_name) {
  // TODO use <algorithm> here..
  for (auto &p : input_ports)
    if ( p.first == port_name ) return p.second;
  for (auto &p : output_ports)
    if ( p.first == port_name ) return p.second;

  assert (false);
  return 0;
}

}
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:



