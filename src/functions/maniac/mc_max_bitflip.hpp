// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
/**
 * @file mc_max_bitflip.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 */

#ifndef MC_MAX_BITFLIP_HPP
#define MC_MAX_BITFLIP_HPP

#include <iostream>
#include <string>
#include <ext-libs/abc/abc_api.hpp>
#include <utils/common_utils.hpp>
#include <functions/maniac/mc_design.hpp>
#include <functions/maniac/mc_make_miter.hpp>
#include <functions/maniac/mc_pdr.hpp>

namespace maniac
{

unsigned long mc_max_bitflip (const mc_design &golden, const mc_design &approx,
			      const mc_port &port_to_check, unsigned signed_outputs,
			      std::string work, bool debug, const unsigned &opt,
			      const unsigned &l_bound, const unsigned &u_bound);


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
