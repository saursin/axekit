
/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_pdr.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_PDR_HPP
#define MC_PDR_HPP

#include <ext-libs/abc/abc_api.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <utils/common_utils.hpp>

namespace maniac
{
bool mc_pdr( const std::string &blif_file, const std::string &out_cex_file,
		 const std::string &results_file, bool debug );
unsigned mc_pdr_orig ( const std::string &blif_file, const std::string &out_cex_file,
		       const std::string &results_file, bool debug );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
