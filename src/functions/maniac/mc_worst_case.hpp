// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : mc_worst_case.hpp
// @brief  : Maniac procedures for sequential verification.
//------------------------------------------------------------------------------

#ifndef MC_WORST_CASE_HPP
#define MC_WORST_CASE_HPP

#include <iostream>
#include <string>
#include <ext-libs/abc/abc_api.hpp>
#include <utils/common_utils.hpp>
#include <functions/maniac/mc_design.hpp>
#include <functions/maniac/mc_pdr.hpp>
#include <functions/maniac/mc_make_miter.hpp>

namespace maniac
{

unsigned long mc_worst_case (const mc_design &golden, const mc_design &approx,
			     const mc_port &port_to_check, unsigned signed_outputs,
			     std::string work, bool debug, const unsigned &opt);


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
