// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : path_routines.cpp
// @brief  : Algorithms on Paths. (longest-path, sort paths'list)
//           A Path is {PI, <set-of-nodes>, PO}
//------------------------------------------------------------------------------

#include "path_routines.hpp"

namespace axekit
{

//------------------------------------------------------------------------------
// Return a partial path in case of any errors.

// TODO - iterate based on smallest path distance, not longest
// MODIFIED, COMPILE AND SEE THE RESULT.
Path get_longest_path_to_po (Object &po) {
  assert ( 1 == abc::Abc_ObjIsPo (po) );
  Object pi = po; // Assign something which we r sure not PI.
  Object driver = abc::Abc_ObjFanin0 (po);
  //----  if ( nullptr != driver)
  //----    std::cout << "First fanin of " << abc::Abc_ObjName (i)
  //----	      << "  :: " << driver->Id << std::endl;
  
  auto track = 0u;
  std::vector <Object> path_nodes;
  while ( nullptr != driver ) {
    if ( 1000 ==  track++ ) {
      std::cout << "[e] Error: get_longest_path_to_po() : "
		<< "Deeeep graph or infinite Loop !" << std::endl;
      break; // Not normal
    }
    if (nullptr == driver) break; // Not normal
    path_nodes.emplace_back (driver);
    if ( abc::Abc_ObjIsPi(driver) ) {
      pi = driver;
      break; // Normal
    }
    if ( abc::Abc_AigNodeIsConst(driver) ) {
      pi = driver;
      break; // Normal
    }
    
    Object fanin0 = abc::Abc_ObjFanin0 (driver);
    Object fanin1 = abc::Abc_ObjFanin1 (driver);
    if ( (NULL == fanin0) || (NULL == fanin1) ) break;
    if ( abc::Abc_ObjIsPi (fanin0) && abc::Abc_ObjIsPi (fanin1) ) {
      pi = fanin0; // Just assign one of those.
      break; // Normal
    }
									  
    if ( abc::Abc_ObjIsPi (fanin0) ) { // Normal
      driver = fanin1;
      continue;
    }
    if ( abc::Abc_ObjIsPi (fanin1) ) { // Normal
      driver = fanin0;
      continue;
    }
    if ( (fanin1->Id) < (fanin0->Id) ) driver = fanin1;
    else driver = fanin0;
  } // end of while.

  if (1 != abc::Abc_ObjIsPi(pi) ) {
    // so many of these warnings. ignore for time being.
    //std::cout << "[w] Path not traced to PI (constant driver) - "
    //          << "get_longest_path_to_po()\n";
  }
  Path longest_path = make_tuple ( pi, path_nodes, po );
  return longest_path;
}

//------------------------------------------------------------------------------
bool sort_paths_predicate ( const Path &a, const Path &b ) {
  if ( (get_nodes_of_path (a)).size() > (get_nodes_of_path (b)).size() ) return true;
  else return false;
}

//------------------------------------------------------------------------------
// In place sorting (for full sort, use n=size of the vector passed)
void sort_paths (std::vector <Path> & paths_list, unsigned &n) {
  n = std::min ( n, (unsigned) paths_list.size() );
  std::partial_sort ( paths_list.begin(), paths_list.begin() + n,
		      paths_list.end(), sort_paths_predicate );
}

//------------------------------------------------------------------------------
// Merge the paths which have same length, but keep the order in the list.
// Note: Do only after sorting/rsorting.
// We will attempt only 3 times. i.e. same length criteria is checked for only 3 times.
// Well even, 2 is sufficient.
// this is not an exhaustive merge algo. Only solves the problem in hand.
// paths_list (#nodes_in_each_path) :: {6, 6, 6, 6, 5, 5, 4, 4, 1, 1, 1}
//             => {24, 10, 8, 1, 1, 1}
// Each path in the result no more corresponds to a single path in the design
// rather its a mix of same node length paths.
// Particularly wrong is the PI-PO list in Path.
//
// Dangerous routine and ofcourse super inefficient.
void merge_same_length_paths ( std::vector <Path> &paths_list ) {
  assert ( (paths_list.size() > 0)  &&  "Empty paths list send!" );
  // idea is to mark everything from the first path to last same-length path.
  // and repeat this 3 times where it ended.

  // ------------------------ Step 1 ------------------------
  //auto first_path = paths_list.at(0);
  auto first_path = paths_list[0];
  auto sec_path_index = 1u;
  auto first_path_length = (get_nodes_of_path (first_path)).size();
  unsigned end_index =  paths_list.size() - 1; // initialize with max possible.
  for (auto i=sec_path_index; i < paths_list.size(); i++ ) { // mark upto end index
    auto path = paths_list.at(i);
    auto length = (get_nodes_of_path (path)).size();
    if (length != first_path_length) {
      end_index = i;
      break;
    }
  }
  for (auto i=sec_path_index; i<end_index; i++) { // now cat all nodes to first path
    //std::cout << "Step 1 : Adding paths from " << i << "th path to path " << 0 << "\n";
    auto curr_path = paths_list.at(i);
    for (auto &node : get_nodes_of_path (curr_path)) {
      add_node_to_path (first_path, node); // there will be duplicates.
    }
  }
  paths_list[0]  = first_path;
  //---for (auto i=sec_path_index + 1; i <end_index; i++) { // and delete remaining paths, if any
  //---  paths_list.erase (i); // todo: use iterators.
  //---}
  //std::cout << "Step 1 : deleting paths from " << sec_path_index << " to " << end_index - 1<< "\n";
  paths_list.erase ( paths_list.begin()+sec_path_index, paths_list.begin()+end_index );
  //---std::cout << "Step 1 : path 0 after addition :: ";
  //---for (auto &i : get_nodes_of_path (paths_list[0]))
  //---  std::cout << get_obj_name (i) << "  ";
  //---std::cout << "\n";
  
  // -------------------------- Step 2 -----------------------
  end_index = 1; // new end_index here, just after first path.
  if (paths_list.size() > end_index) return; // nothin to do. all following paths accounted.
  first_path = paths_list.at (end_index); //shudnt crash, end_index guaranteed to be there.
  sec_path_index = end_index + 1;
  first_path_length = (get_nodes_of_path (first_path)).size();
  end_index =  paths_list.size() - 1; // initialize with max possible.
  for (auto i=sec_path_index; i < paths_list.size(); i++ ) { // mark upto end index
    auto path = paths_list.at(i);
    auto length = (get_nodes_of_path (path)).size();
    if (length != first_path_length) {
      end_index = i;
      break;
    }
  }
  //shudnt crash.
  for (auto i=sec_path_index; i<end_index; i++) { // now cat all nodes to first path
    auto curr_path = paths_list.at(i);
    for (auto &node : get_nodes_of_path (curr_path)) {
      add_node_to_path (first_path, node); // and there will be duplicates.
    }
  }
  //---for (auto i=sec_path_index + 1; i <end_index; i++) { // and delete remaining paths, if any
  //---  paths_list.erase (i); // todo: use iterators.
  //---}
  paths_list[1]  = first_path;
  paths_list.erase ( paths_list.begin()+sec_path_index, paths_list.begin()+end_index );

  std::cout << "Step 2 : path 0 after addition :: ";
  for (auto &i : get_nodes_of_path (first_path))
    std::cout << abc_ntk_inline::get_obj_name (i) << "  ";
  std::cout << "\n";

  // -------------------------- Step 3 -----------------------
  end_index = 2; // new end_index is just after second path.
  if (paths_list.size() > end_index) return; // nothin to do. all following paths accounted.
  first_path = paths_list.at (end_index); //shudnt crash, end_index guaranteed to be there.
  sec_path_index = end_index + 1;
  first_path_length = (get_nodes_of_path (first_path)).size();
  end_index =  paths_list.size() - 1; // initialize with max possible.
  for (auto i=sec_path_index; i < paths_list.size(); i++ ) { // mark upto end index
    auto path = paths_list.at(i);
    auto length = (get_nodes_of_path (path)).size();
    if (length != first_path_length) {
      end_index = i;
      break;
    }
  }
  //shudnt crash.
  for (auto i=sec_path_index; i<end_index; i++) { // now cat all nodes to first path
    auto curr_path = paths_list.at(i);
    for (auto &node : get_nodes_of_path (curr_path)) {
      add_node_to_path (first_path, node); // and there will be duplicates.
    }
  }
  //---for (auto i=sec_path_index + 1; i <end_index; i++) { // and delete remaining paths, if any
  //---  paths_list.erase (i); // todo: use iterators.
  //---}
  paths_list[2]  = first_path;
  paths_list.erase ( paths_list.begin()+sec_path_index, paths_list.begin()+end_index );
  
  std::cout << "Step 3 : path 0 after addition :: ";
  for (auto &i : get_nodes_of_path (first_path))
    std::cout << abc_ntk_inline::get_obj_name (i) << "  ";
  std::cout << "\n";

}

//------------------------------------------------------------------------------
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
