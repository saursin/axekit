// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yig_conversion.cpp
// @brief  : yig <-> gia, yig <-> ntk conversions
//------------------------------------------------------------------------------

#include "yig_conversion.hpp"
#include <string>

namespace abc {

//---int Gia_ManPoIsConst0( Gia_Man_t * p, int iPoIndex )    {
//---  return Gia_ManIsConst0Lit( Gia_ObjFaninLit0p(p, Gia_ManPo(p, iPoIndex)) );
//---}
//---int Gia_ManPoIsConst1( Gia_Man_t * p, int iPoIndex )    {
//---  return Gia_ManIsConst1Lit( Gia_ObjFaninLit0p(p, Gia_ManPo(p, iPoIndex)) );
//---}


// The above 2 functions in gia.h is the base of these routines.
inline int Gia__ManObjIsConst0 ( Gia_Man_t * gia, Gia_Obj_t *pObj )    {
  auto num = abc::Gia_ObjNum (gia, pObj); // this one or ID?
  //auto num = abc::Gia_ObjId (gia, pObj); 
  return Gia_ManIsConst0Lit( Gia_ObjFaninLit0p (gia, Gia_ManObj(gia, num)) );
}
inline int Gia__ManObjIsConst1( Gia_Man_t * gia, Gia_Obj_t *pObj)    {
  auto num = abc::Gia_ObjNum (gia, pObj); // this one or ID?
  //auto num = abc::Gia_ObjId (gia, pObj); 
  return Gia_ManIsConst1Lit( Gia_ObjFaninLit0p (gia, Gia_ManObj(gia, num)) );
}


inline bool is_fanin0_complement ( Gia_Obj_t *pObj ) {
  return pObj->fCompl0 == 1;
}

inline bool is_fanin1_complement ( Gia_Obj_t *pObj ) {
  return pObj->fCompl1 == 1;
}

}

namespace axekit
{

namespace {
auto cnt = 0;
}

inline bool is_obj_in_ymap ( std::map <abc::Gia_Obj_t*, Yig::Yig_t> &ymap,
			     abc::Gia_Obj_t *obj ) {
  return ymap.count ( obj ) == 1; // shd be logarithmic to check.
}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

// Note: this procedure modifies GIA. gia is functionally not equivalent.
void add_abcObj_to_yig ( abc::Gia_Man_t *gia, abc::Gia_Obj_t *pObj, Yig::YigGraph_t &yig,
			 std::map <abc::Gia_Obj_t *, Yig::Yig_t> &ymap ) {

  if ( is_obj_in_ymap (ymap, pObj) ) return;

  const auto num_fanins = abc::Gia_ObjFaninNum (gia, pObj);
  assert ( !abc::Gia_ObjIsMux (gia, pObj ) ); // lets get back to this later.
  assert ( num_fanins < 3  ); // ignore all sunaplis.
  assert ( num_fanins != 0 ); // PI and const shd be already added.
  assert ( !abc::Gia_ObjIsPi (gia, pObj) ); // shd be already entered.
  assert ( !abc::Gia_ObjIsXor (pObj) ); // lets get back to this later.

  if ( 1 == num_fanins ) { // PO or inv output. BUF shd nt be there, though nt a problem.
    // we add only the driver here, no yig gate.
    auto fan0 = abc::Gia_ObjFanin0 (pObj);
    add_abcObj_to_yig ( gia, fan0, yig, ymap );
    auto yfan0 = ymap.at (fan0);
    if (abc::is_fanin0_complement ( pObj )) { yfan0 = !yfan0; }
    ymap[pObj] = yfan0;
    return;
  }
  
  auto fan0 = abc::Gia_ObjFanin0 (pObj);
  auto fan1 = abc::Gia_ObjFanin1 (pObj);

  add_abcObj_to_yig ( gia, fan0, yig, ymap );
  add_abcObj_to_yig ( gia, fan1, yig, ymap );

  auto yfan0 = ymap.at (fan0);
  auto yfan1 = ymap.at (fan1);
  if (abc::is_fanin0_complement ( pObj )) { yfan0 = !yfan0; }
  if (abc::is_fanin1_complement ( pObj )) { yfan1 = !yfan1; }
  auto node = Yig::add_and ( yig, yfan0, yfan1 );
  std::cout << "Yig: added AND " << node_name (yig, node)
	    << " " << node_name (yig, yfan0) << " & " << node_name (yig, yfan1) << "\n";
  ymap [pObj] = node;
}



Yig::YigGraph_t gia_to_yig ( abc::Gia_Man_t *orig, const unsigned num_nodes ) {
  auto gia  = abc::Gia_ManDup (orig);
  gia = abc::Gia_ManRehash (gia, 1);
  Yig::YigGraph_t yig;
  Yig::start (yig, abc::Abc_UtilStrsav (gia->pName), num_nodes);
  
  abc::Gia_Obj_t * poObj = nullptr;
  abc::Gia_Obj_t * piObj = nullptr;
  int i, j;
  
  // TODO: consider smart ponters here
  // http://stackoverflow.com/questions/10333854/how-to-handle-a-map-with-pointers
  std::map <abc::Gia_Obj_t *, Yig::Yig_t> ymap;

  // 0. initialize the map with constants.
  ymap [ abc::Gia_ManConst0(gia) ] = Yig::zero (yig);
  ymap [ abc::Gia_ManConst1(gia) ] = Yig::one  (yig);
  
  // 1. add all PIs
  Gia_ManForEachPi (gia, piObj, i)
  {
    std::string name = "i" + std::to_string(i);
    if ( gia->vNamesIn ) {
      auto ch = (char*)abc::Vec_PtrEntry (gia->vNamesIn, i);
      name = std::string (ch);
    }
    auto pi = Yig::add_pi (yig, name);
    ymap [piObj] = pi;

  }

  std::cout << "Yig PIs : ";
  for ( auto &pi : Yig::all_pi_nodes (yig) ) {
    std::cout << Yig::node_name (yig, pi) << " ";
  }
  std::cout << "\n";
  std::cout << "num_vertices = " << num_vertices (yig) << "\n" ;
  
  // 2. start adding from the POs, recursively.
  Gia_ManForEachPo (gia, poObj, i)
  {
    std::string name = "o" + std::to_string(i);
    if ( gia->vNamesOut ) {
      auto ch = (char*)abc::Vec_PtrEntry (gia->vNamesOut, i);
      name = std::string (ch);
    }

    std::cout << "--------------- For PO " << name << " --------------- \n";
    if ( abc::Gia_ManPoIsConst0 (gia, i) ) {
      Yig::add_po ( yig, Yig::zero(yig), name );
      continue;
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i) ) {
      Yig::add_po ( yig, Yig::one(yig), name );
      continue;
    }
    
    add_abcObj_to_yig (gia, poObj, yig, ymap);
    Yig::add_po ( yig, ymap.at (poObj), name );
    std::cout << "Added po " << name  << "  num_vertices = " << num_vertices (yig) << "\n";

  }

  //std::exit (0);

  auto &info = boost::get_property ( yig, boost::graph_name );
  std::cout << "size of Yig = " << info.strash.size() << "\n";
  std::cout << "num vertices of Yig = " << num_vertices (yig) << "\n";
  
  write ("dummy.yag", yig);
  std::cout << "Success!!!\n";
  std::exit (0);
  assert (false && "Need to do gia manstop");
  return yig;
}


//------------------------------------------------------------------------------
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
