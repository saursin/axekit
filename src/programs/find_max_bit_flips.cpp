// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : find_max_bit_flips.cpp
// @brief  : input is a verilog RTL or a BLIF or an AIG, golden and approx
//           Finds the max bit flips in approx for a given port as a result 
//           of approximation
//------------------------------------------------------------------------------

#include "find_max_bit_flips.hpp"

using namespace axekit;

//------------------------------------------------------------------------------
namespace { // these are our program options.
program_options opts;

std::string golden, approx;
std::string top;
std::string report = "kakakikikoo.rpt";
auto debug = 1u;
std::string check_port;

}


//------------------------------------------------------------------------------
// Some helper functions moved out of main.

int parse_and_check_options (int argc, char **argv) {
  using boost::program_options::value;
  
  opts.add_options() 
    ( "golden,g",  value ( &golden ),  "Input Golden Verilog/BLIF/AIG" )
    ( "approx,a",  value ( &approx ),  "Input Approx Verilog/BLIF/AIG" )
    ( "top,t",     value ( &top ),    "Name of the TOP Module" )
    ( "check_port,p",   value ( &check_port ), "Port to Check" )
    ( "report,r",       value_with_default ( &report ), "Output report" )
    ( "debug,d",        value_with_default ( &debug ),  "Extra debugging levels" )
    ;
  
  try {
      opts.parse( argc, argv );
  } 
  catch (std::exception &e) {
    std::cout << "[e] Error in options provided - " << e.what() << "\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -1;
  }
  if ( opts.is_set("help") ) {
    std::cout << opts << std::endl;
    return 0;
  }
  if ( !opts.good() || !opts.is_set("golden") || !opts.is_set("approx") || 
       !opts.is_set("top") || !opts.is_set("check_port") ) {
    std::cout << "[e] Error: Bad options!\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -2;
  }
  if (debug > 0) {
    std::cout << "[i] start_time   = " << curr_time();
    std::cout << "[i] golden       = " << golden << "\n";
    std::cout << "[i] approx       = " << approx << "\n";
    std::cout << "[i] top          = " << top << "\n";
    std::cout << "[i] check_port   = " << check_port << "\n";
    std::cout << "[i] report       = " << report << "\n";
  }
  if (debug > 1) {
    std::cout << "[i] debug_level  = " << debug << "\n";
  }

  return 0; // normal value
}


//------------------------------------------------------------------------------
int main (int argc, char **argv) {
  auto opts_status = parse_and_check_options (argc, argv);
  if (0 != opts_status) return opts_status;

  //-----------------------------------------------------------
  auto begin_time = std::clock();
  const std::string work = "work_bitflip";
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; 
  //-----------------------------------------------------------
  // 1. Get the corresponding networks
  start_yosys ();
  auto golden_ext = get_extension_of_file (golden);
  auto approx_ext = get_extension_of_file (approx);
  std::string golden_blif, approx_blif;
  unsigned width = 0;
  Port port_to_check;

  // Get the Golden Blif
  if ( ".aig" == golden_ext)  {
    // TODO: get these functionalities
    assert (false);
  }
  else if (".v" == golden_ext) {
    golden_blif = work_path + "gold.blif";
    verilog_to_blif_in_ys (golden_blif, golden, top);
    width = get_port_width_in_ys ( check_port );
    port_to_check = make_port (check_port, width);
  }
  else if (".blif" == golden_ext) {
    golden_blif = golden;
    // TODO: get these functionalities
    assert (false && "With BLIF we cannot know the width.");
  } else {
    assert (false && "Unknown file format for golden");
  }
  //print_dbg ("here");
  // Get the Approx Blif
  if ( ".aig" == approx_ext)  {
    // TODO: get these functionalities
    assert (false);
  }
  else if (".v" == approx_ext) {
    approx_blif = work_path + "approx.blif";
    verilog_to_blif_in_ys (approx_blif, approx, top);
    width = get_port_width_in_ys ( check_port );
    port_to_check = make_port (check_port, width);
  }
  else if (".blif" == approx_ext) {
    approx_blif = approx;
    // TODO: get these functionalities
    assert (false && "With BLIF we cannot know the width.");
  } else {
    assert (false && "Unknown file format for approx");
  }

  
  //-----------------------------------------------------------
  // 2. Fire up ABC 
  Frame frame  = start_abc();
  Network golden_ntk  = read_blif_to_network (golden_blif);
  Network approx_ntk  = read_blif_to_network (approx_blif);
  assert ( is_network_good (golden_ntk) && is_network_good (approx_ntk));

  // Miter appends _appx to "approx_design" name.
  // Solver writes out "approx_network".
  // To avoid mismatch, approx_design should NOT have _appx, but approx_ntk should have this.
  // 
  auto golden_design = make_design_from_network (golden_ntk); 
  auto approx_design = make_design_from_network (approx_ntk); 
  set_network_name (approx_ntk, get_network_name (approx_ntk) + "_appx");

  auto golden_verilog = work_path + "gold.v";
  write_verilog_from_network (golden_verilog, golden_ntk);

  auto max_bit_flips = binsearch_max_bit_flip ( golden_verilog, approx_design,
						approx_ntk, port_to_check,
						work_path, debug ); 

  auto time_total = get_elapsed_time ( begin_time );
  //return 0;

  //------------------------------------------------------------
  std::cout << "[i] max bit flip error = " << max_bit_flips << "\n";
  
  std::ofstream rpt;
  rpt.open ( report );
  rpt << std::fixed << std::showpoint << std::setprecision(2);
  rpt << "Maniac Max Bit Flip Error report " << curr_time();
  rpt << "------------------------------------------------------------\n";
  rpt << "Golden circuit file   :: " << golden << "\n";
  rpt << "Approx circuit file   :: " << approx << "\n";
  rpt << "Checked Port          :: " << check_port << std::endl;
  rpt << "Top Module            :: " << top << std::endl;
  rpt << "Max Bit-Flip Error    :: " << max_bit_flips << std::endl;
  rpt << "Total Time (s)        :: " << time_total << "\n";
  rpt.close();

  delete_network ( approx_ntk );
  delete_network ( golden_ntk );
  stop_abc();
  stop_yosys();

  std::cout << "[i] finish_time = " << curr_time();
  return 0;
}



//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
