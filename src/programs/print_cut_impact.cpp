// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : print_cut_impact.cpp
// @brief  : input is a verilog RTL output is a synthesised netlist, not
//           mapped to any technology lib and uses internal pnemonics and
//           as such is useless.
//           Prints the impact of cuts on top-path nodes. (Volume, Size etc)
//------------------------------------------------------------------------------

#include "print_cut_impact.hpp"

using namespace axekit;

//------------------------------------------------------------------------------
namespace { // these are our program options.
program_options opts;

std::string ifile;
std::string top;
std::string report = "kakakikikoo.rpt";
auto debug = 1u;
auto max_cut_size = 3;
std::string check_port;
}


//------------------------------------------------------------------------------
// Some helper functions moved out of main.

int parse_and_check_options (int argc, char **argv) {
  using boost::program_options::value;
  
  opts.add_options() 
    ( "ifile,i",   value ( &ifile ),  "Input Verilog RTL (.v) or BLIF (.blif)" )
    ( "top,t",     value ( &top ),    "Name of the TOP Module" )
    ( "check_port,p",   value ( &check_port ), "Port to Check" )
    ( "report,r",       value_with_default ( &report ), "Output report" )
    ( "debug,d",        value_with_default ( &debug ),  "Extra debugging levels" )
    ( "max_cut_size,m", value_with_default( &max_cut_size ),     "Test Input" )
    ;
  
  try {
      opts.parse( argc, argv );
  } 
  catch (std::exception &e) {
    std::cout << "[e] Error in options provided - " << e.what() << "\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -1;
  }
  if ( opts.is_set("help") ) {
    std::cout << "Maniac : print_cut_impact program : " << curr_time();
    std::cout << "Enumerates cuts in the longest paths and report the impact (size, volume)\n";
    std::cout << opts << std::endl;
    return -3;
  }
  if ( !opts.good() || !opts.is_set("ifile") || !opts.is_set("top") ||  
       !opts.is_set("check_port") ) {
    std::cout << "[e] Error: Bad options!\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -2;
  }
  if (debug > 0) {
    std::cout << "[i] start_time  = " << curr_time();
    std::cout << "[i] ifile       = " << ifile << "\n";
    std::cout << "[i] top         = " << top << "\n";
    std::cout << "[i] check_port  = " << check_port << "\n";
    std::cout << "[i] report      = " << report << "\n";
  }
  if (debug > 1) {
    std::cout << "[i] max_cut_size = " << max_cut_size << "\n";
    std::cout << "[i] debug_level  = " << debug << "\n";
  }

  return 0; // normal value
}


//------------------------------------------------------------------------------
int main (int argc, char **argv) {
  auto opts_status = parse_and_check_options (argc, argv);
  if (0 != opts_status) return opts_status;
  //-----------------------------------------------------------
  auto max_attempts = 10u; // Maximum cut attempts allowed.
  auto applied_cuts = 0u;  // Tracks number of succesful cuts for approx.
  auto n_top_paths = 5u;   // takes only these many paths for approx.

  const std::string work = "work_cut";
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; 
  //-----------------------------------------------------------
  // 1. Get the corresponding blifs.
  std::string iblif;
  auto ext = get_extension_of_file (ifile);
  if ( ".blif" == ext ) {
    iblif = ifile;
  }
  else if ( ".v" == ext ){
    iblif = work_path + "input.blif";
    start_yosys();
    verilog_to_blif_in_ys (iblif, ifile, top);
    //auto width = get_port_width_in_ys ( check_port );
    //auto port_to_check = make_port (check_port, width);
  } else {
    assert (false && " Input not in verilog (.v) or blif (.blif) format!");
  }
  
  //-----------------------------------------------------------
  // 2. Fire up ABC 
  Frame frame  = start_abc();
  Network ntk  = read_blif_to_network (iblif);
  assert ( is_network_good (ntk) );
  auto design = make_design_from_network (ntk); 
  if ( !check_network (ntk) )
    std::cout << "[e] Error. Original Network integrity failed..  :("
	      << std::endl;
  if (debug > 10) print_network_details (ntk, "Original Network :: ");
  auto orig_ntk_name = get_network_name ( ntk );
  auto orig_ntk_node_cnt = get_node_count ( ntk );

  //------------------------------------------------------------
  auto attempts = 0u;
  unsigned t_time_sort_paths = 0u, t_time_apply_cut = 0u, t_time_generate_cuts = 0u;

      
  ntk = strash_network (ntk); 
  assert ( is_topologically_sorted (ntk) );
  // 3. Sort the longest paths.
  if (debug > 1) std::cout << "[i] Sorting the longest paths begin :: " << curr_time();
  auto begin_time = std::clock ();
  auto po_list = get_po_list (ntk);
  auto nTop = std::min ( n_top_paths, (unsigned int)po_list.size() );
  std::vector <Path> paths_list;
  for (auto &po : po_list) { // TODO: interested only in nTop paths.
    auto longest_path = get_longest_path_to_po (po);
    // prune const drivers to PO
    if ( is_node_const ( get_pi_of_path (longest_path) ) ) continue;
    paths_list.emplace_back (longest_path);
  }

  sort_paths ( paths_list, nTop);
  auto time_sort_paths = get_elapsed_time ( begin_time );
  t_time_sort_paths += time_sort_paths; // total time
  if (debug > 1) {
    std::cout << "[i] Sorting the longest paths end :: " << curr_time();
    std::cout << "[i] Sorting time = " << time_sort_paths << std::endl;  
  }
  if (debug > 10) std::cout << nTop << " Top Paths in the sorted order \n";
  if (debug > 10) print_paths (paths_list); // Print longest paths in the order.
  //------------------------------------------------------------
  // Do basic reporting before the cut enumeration.
  std::ofstream rpt;
  rpt.open ( report );
  rpt << std::fixed << std::showpoint << std::setprecision(2);
  rpt << "Maniac Approximate Synthesis report " << curr_time();
  rpt << "------------------------------------------------------------\n";
  rpt << "Circuit             :: " << orig_ntk_name << std::endl;
  rpt << "Circuit Nodes       :: " << orig_ntk_node_cnt << std::endl;
  rpt << "Circuit File        :: " << ifile << std::endl;
  rpt << "Checked Port        :: " << check_port << std::endl;
  rpt << "Top Module          :: " << top << std::endl;
  rpt << "----------------CUT REPORT--------------------" << std::endl;


  // Get a flattend list of nodes to cut based on 1) longest path & 2) cut-volume.
  // 4. Enumerate the cuts and print the results.
  if (debug > 1) std::cout << "[i] Cut generation begin :: " << curr_time();
  begin_time = std::clock();
  auto cut_mgr = start_cut_manager (max_cut_size);
  std::vector <Object> node_list;
  for (auto i = 0u; i < nTop; i++) {
    rpt << "Worst Path - " << i << "\n";
    if (debug > 0) std::cout << "[i] Worst Path - " << i << "\n";
    print_cut_volume_list ( cut_mgr, paths_list[i], rpt, debug );
  }
  if (debug > 1) std::cout << "[i] Cut generation end :: " << curr_time();
  auto time_generate_cuts = get_elapsed_time ( begin_time );
  t_time_generate_cuts += time_generate_cuts; // total time
  if (debug > 1) std::cout << "[i] Cut generation time = " 
			   << time_generate_cuts << std::endl;
  
  //------------------------------------------------------------  
  rpt << "---------------------------------------------" << std::endl;
  rpt << "Path Sorting Time (s)    :: " << t_time_sort_paths << std::endl;
  rpt << "Cut Apply Time (s)       :: " << t_time_apply_cut << std::endl;
  rpt << "Cut Enumeration Time (s) :: " << t_time_generate_cuts << std::endl;
  rpt << "Total Time (s)           :: "
      << (t_time_sort_paths + t_time_apply_cut + t_time_generate_cuts) << std::endl;
  rpt.close();
  delete_network ( ntk );
  stop_abc();
  stop_yosys();
  std::cout << "[i] finish_time = " << curr_time();
  return 0;
}



//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
