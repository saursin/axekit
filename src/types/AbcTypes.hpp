// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : AbcTypes.hpp
// @brief  : Typedefs and inline routines for different Abc datatypes.
//           Purpose is to hide these "pointers" as much as possible.
//           Cannot wrapup with Classes for performance. (Going to have millions of instances)
//
//------------------------------------------------------------------------------
#pragma once
#ifndef ABCTYPES_HPP
#define ABCTYPES_HPP

#include <ext-libs/abc/abc_api.hpp>


// Some extra constructs for the abc namespace.
namespace abc {
//------------------------------------------------------------------------------
// This struct is used internally for IvyProove, with the default values.
struct Prove_ParamsStruct_t_  {
  // general parameters
  int     fUseFraiging = 1;    // enables fraiging
  int     fUseRewriting = 1;   // enables rewriting
  int     fUseBdds = 0;        // enables BDD construction when other methods fail
  int     fVerbose = 0;        // prints verbose stats
  // iterations
  int     nItersMax = 6;       // the number of iterations
  // mitering 
  int     nMiteringLimitStart = 5000;  // starting mitering limit
  float   nMiteringLimitMulti = 2.0;   // multiplicative coefficient to increase the limit in each iteration
  // rewriting 
  int     nRewritingLimitStart = 3;  // the number of rewriting iterations
  float   nRewritingLimitMulti = 1.0; // multiplicative coefficient to increase the limit in each iteration
  // fraiging 
  int     nFraigingLimitStart = 2;   // starting backtrack(conflict) limit
  float   nFraigingLimitMulti = 8.0;   // multiplicative coefficient to increase the limit in each iteration
  // last-gasp BDD construction
  int     nBddSizeLimit = 1000000;   // the number of BDD nodes when construction is aborted
  int     fBddReorder = 1;           // enables dynamic BDD variable reordering
  // last-gasp mitering
  int     nMiteringLimitLast = 0;    // final mitering limit
  // global SAT solver limits
  abc::ABC_INT64_T  nTotalBacktrackLimit = 0;  // global limit on the number of backtracks
  abc::ABC_INT64_T  nTotalInspectLimit = 0;    // global limit on the number of clause inspects
  // global resources applied
  abc::ABC_INT64_T  nTotalBacktracksMade;  // the total number of backtracks made
  abc::ABC_INT64_T  nTotalInspectsMade;    // the total number of inspects made
};
using Prove_Params_t = Prove_ParamsStruct_t_;

enum class ProveResult { UNDECIDED = -1, NOT_EQUIVALENT = 0, EQUIVALENT = 1, ERROR = -2 };

}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
namespace axekit {

// Various ABC datatypes. 
using Network = abc::Abc_Ntk_t * ;
using Object  = abc::Abc_Obj_t * ;
using Frame   = abc::Abc_Frame_t *;
using CutManager = abc::Abc_ManCut_t * ;
using ObjVector  = abc::Vec_Ptr_t * ;
using ProveParameters = abc::Prove_Params_t;

namespace abc_ntk_inline {
//------------------------------------------------------------------------------
inline bool is_network_good ( Network ntk ) {
  return (ntk != nullptr);
}
//------------------------------------------------------------------------------
inline bool is_object_good ( Object obj ) {
  return (obj != nullptr);
}
//------------------------------------------------------------------------------
inline std::string get_network_name ( Network ntk ) {
  return std::string ( ntk->pName );
}
//------------------------------------------------------------------------------
inline void set_network_name ( Network ntk, std::string new_name ) {
  assert ( is_network_good(ntk) );
  ntk->pName = abc::Extra_UtilStrsav ( (char *) new_name.c_str() );
}
//------------------------------------------------------------------------------
inline void delete_network ( Network ntk ) {
  if ( !is_network_good (ntk) ) {
    std::cout << "[e] Error: delete_network() : Empty network" << std::endl;
    return;
  }
  abc::Abc_NtkDelete (ntk);
}
//------------------------------------------------------------------------------
inline Object get_const1 ( Network ntk ) {
  assert ( is_network_good (ntk) );
  return abc::Abc_AigConst1 (ntk);
}
//------------------------------------------------------------------------------
inline Object get_const0 ( Network ntk ) { // TODO: to complete
  assert (false);
  // compliment of const1 
  return nullptr;
}
//------------------------------------------------------------------------------
inline bool check_network ( Network ntk ) {
  return   ( 1 == abc::Abc_NtkCheck( ntk) ) ;
}
//------------------------------------------------------------------------------
// strash always. else sometimes seg fault after succesful approximation.
inline Network  copy_network ( Network  ntk ) {
  assert ( is_network_good (ntk) );
  return abc::Abc_NtkDup ( abc::Abc_NtkStrash (ntk, 1, 1, 0) );
}
//------------------------------------------------------------------------------
inline char * get_obj_name ( Object obj ) {
  assert ( is_object_good (obj) );
  return abc::Abc_ObjName (obj);
}
//------------------------------------------------------------------------------
inline int get_obj_vector_size (ObjVector obj_vector) {
  return abc::Vec_PtrSize (obj_vector);
}

}

//------------------------------------------------------------------------------
} // namespace axekit



#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
