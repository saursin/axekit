// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cnf_utils.hpp
//           Utilties for cnf writing and reading.
//------------------------------------------------------------------------------
#pragma once

#ifndef CNF_UTILS_HPP
#define CNF_UTILS_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <algorithm>

#include <boost/dynamic_bitset.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <sat/cnf/cnf.h>
#include <sat/bsat/satSolver.h>
#include <ext-libs/abc/abc_api.hpp>

namespace axekit {

// Constraints the output bits as in output_pattern in CNF.
// Returns the number of inputs NOT used inside CNF.
// (2 ** return_val) has to be multiplied for model counting.
int write_cnf ( abc::Cnf_Dat_t *p, char *fileName, const int fReadable,
		const int num_inputs, const int num_outputs,
		const boost::dynamic_bitset<> &output_pattern );


// This CNF should be used ONLY for ABC internal purpose.
void write_cnf ( abc::Cnf_Dat_t *p, char *fileName, int fReadable, int num_inputs );


// constraints all outputs to 1. Use the bitset function otherwise.
// should not have const-0 outputs in outputs.
// Returns the number of inputs NOT used inside CNF.
// (2 ** return_val) has to be multiplied for model counting.
int write_cnf ( abc::Cnf_Dat_t *p, char *fileName, const int fReadable,
		const int num_inputs, const int num_outputs );

}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
