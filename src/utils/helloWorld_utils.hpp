// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : helloWorld_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef HELLOWORLD_UTILS_HPP
#define HELLOWORLD_UTILS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/dynamic_bitset.hpp>
#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>

namespace axekit {
  
//---void create_work_dir (const std::string &dir_name);
//---void delete_work_dir (const std::string &dir_name);
//---void run_abc_cmd (abc::Abc_Frame_t *frame, std::string cmd, bool debug);
//---void optimize_with_yosys ();
//---void optimize_with_abc (abc::Abc_Frame_t *frame);

}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
