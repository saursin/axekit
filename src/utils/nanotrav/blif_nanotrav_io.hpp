// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : blif_nanotrav_io.hpp
// @brief  : read blif (TODO: write_blif also) using Cudd Nanotrav
//
//------------------------------------------------------------------------------
#pragma once

#ifndef BLIF_NANOTRAV_IO_HPP
#define BLIF_NANOTRAV_IO_HPP

#include <iostream>
#include <cstdio>
#include <vector>
#include <utility>
#include <string>

#include <cuddInt.h>
#include <cirkit/bdd_utils.hpp>
#include "ntr.hpp"
#include "ntr_extra.hpp"

namespace axekit {

cirkit::bdd_function_t read_nanotrav_blif (const std::string &file, Cudd &cudd);
bool write_nanotrav_blif ( const std::string &file, const cirkit::bdd_function_t &bddf,
			   const std::string &top_module = "dummy" );
bool write_nanotrav_blif ( const std::string &file, const cirkit::bdd_function_t &bddf,
			   nanotrav::BnetNetwork* const &network );


}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
