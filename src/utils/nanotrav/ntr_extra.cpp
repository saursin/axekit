// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : ntr_extra.cpp
// @brief  : extra utilities for reading and writing blif.
//------------------------------------------------------------------------------

#include "ntr_extra.hpp"


namespace axekit {

using namespace nanotrav;

// copis the inputs, outputs and symbol-table only.
// to copy thew DdNodes use copy_DdInputs_to_bnet()

BnetNetwork * copy_bnet (BnetNetwork *ntk) {

  BnetNetwork *new_ntk;
  assert (false); 
  //---new_ntk = ALLOC(BnetNetwork,1);
  //---new_ntk->npis     = ntk->npis;
  //---new_ntk->ninputs  = ntk->ninputs;
  //---new_ntk->inputs   = ntk->inputs;
  //---new_ntk->npos     = ntk->npos;
  //---new_ntk->noutputs = ntk->npis;
  //---new_ntk->outputs  = ntk->outputs;
  //---new_ntk->nlatches = ntk->nlatches;
  //---new_ntk->npis = ntk->npis;
  //---new_ntk->npis = ntk->npis;
  //---new_ntk->npis = ntk->npis;


  return new_ntk;
}

// associates the existing input nodes in the dd to the names in ntk
// if there are more inputs in the ntk than in the dd, create new vars in the DD.
bool copy_DdInputs_to_bnet (DdManager *dd, BnetNetwork *ntk) {

  assert (ntk != nullptr);
  assert (dd  != nullptr);
  
  const auto max_inputs = Cudd_ReadSize (dd); // only these many inputs are available in DdManger.

  // 1. search the st_table of ntk, for the input bnet nodes of ntk.
  // 2. for each of these input-bnet node, find correspoding ddnode pointer.
  // 3. assign input-bnet-dd pointer the valid input bdd from DD.
  BnetNode *node;
  for (auto i=0; i < ntk->npis; i++) {
    if (i >= max_inputs) {
      //std::cout << "[i] Creating more input variables in DD\n";
    }
    if ( !st_lookup(ntk->hash, ntk->inputs[i], (void **)&node) ) {
      std::cout << "[e] input-name not present in the symbol-table!\n";
      return false;
    }
    // node->dd = Cudd_ReadVars (dd, i); // does not create a new var
    node->dd = Cudd_bddIthVar (dd, i); // creates a new var if it does not exist.
    if (node->dd == nullptr) {
      std::cout << "[e] Error in reading var " << i << " from Cudd \n";
      return false;
    }
    Cudd_Ref ( node->dd );  // do we really need to increase the reference count?
    
  }

  if ( ntk->npis > max_inputs ) {
    std::cout  << "[i] Nanotrav extended CUDD with " << (ntk->npis - max_inputs) << " inputs \n";
  }
  
  return true;
}


// The existing input nodes in DD are fully shared position-wise.
int Ntr_build_shared_DDs (
  BnetNetwork * net /**< network for which DDs are to be built */,
  DdManager * dd /**< %DD manager */,
  NtrOptions * option /**< option structure */) {

  assert (dd != nullptr);
  assert (option != nullptr);
  assert (net != nullptr);

  int status;
  if ( Cudd_ReadSize (dd) > 0 ) { // DD has already some input nodes.
    copy_DdInputs_to_bnet (dd, net);
    status = Ntr_buildDDs (net, dd, option, net);
  }
  else {
    status = Ntr_buildDDs (net, dd, option, NULL);
  }
  
  return status;

} 




} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

