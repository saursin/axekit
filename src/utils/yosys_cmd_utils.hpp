// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yosys_cmd_utils.hpp
// 
// DEPERECIATED. No need to use these routines.
//------------------------------------------------------------------------------
#pragma once

#ifndef YOSYS_CMD_UTILS_HPP
#define YOSYS_CMD_UTILS_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <cstdio>

#include <ext-libs/yosys/yosys_api.hpp>
#include <utils/common_utils.hpp>

namespace axekit {

//------------------------------------------------------------------------------
// All the below ones uses equivalent command line commands.
// Depreciated. Should not use these.
void ys_optimize (bool localdebug);
void ys_verilog_to_blif (std::string iverilog, std::string oblif, std::string top_module,
			 bool keep = true, bool local_debug = false);
void ys_mixed_to_blif (std::string iverilog, std::string iblif1, std::string iblif2,
		       std::string oblif, std::string top_module, bool localdebug = false);
void ys_blif_to_verilog (std::string iblif, std::string overilog,
			 std::string top_module);
unsigned ys_get_port_width ( const std::string &port_name );

//------------------------------------------------------------------------------
}


#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
