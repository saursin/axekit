// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yosys_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef YOSYS_UTILS_HPP
#define YOSYS_UTILS_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <cstdio>

#include <ext-libs/yosys/yosys_api.hpp>
#include <utils/common_utils.hpp>

namespace axekit {

void start_yosys ();
void stop_yosys ();
bool is_yosys_started ();
void parse_verilog_to_ys (const std::string &filename, const std::string &top_module);
void write_verilog_from_ys (std::string filename);
void write_blif_from_ys (std::string filename);

std::string get_top_module_in_ys ();

// Use with caution, uses command line way.
void convert_1v_and_2b_to_blif ( std::string ivlog, std::string iblif1, std::string iblif2,
				 std::string oblif, std::string top_module );

unsigned get_port_width_in_ys ( const std::string &port_name );
void verilog_to_blif_in_ys (const std::string &oblif, const std::string &ivlog, 
			    const std::string &top_module);

Yosys::RTLIL::Design *abc_parse_blif2 (std::string filename);

//------------------------------------------------------------------------------
}


#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
